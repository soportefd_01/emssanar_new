//import 'dart:async';

import 'package:emssanar_new/helpers/verpagina.dart';
import 'package:emssanar_new/pages/citaspage.dart';
import 'package:emssanar_new/pages/estadoAutorizaciones.dart';
import 'package:emssanar_new/pages/eventospage.dart';
import 'package:emssanar_new/pages/interaccionapppage.dart';
import 'package:emssanar_new/pages/prestadorespacientepage.dart';
//import 'package:emssanar_new/pages/redserviciospage.dart';
import 'package:emssanar_new/pages/rutasatencionpage.dart';
import 'package:emssanar_new/pages/sections/submenuEstandar.dart';
import 'package:emssanar_new/pages/sections/submenueventos.dart';
import 'package:emssanar_new/pages/sections/submenuinfopersonal.dart';
import 'package:emssanar_new/pages/sections/submenuredservicios.dart';
import 'package:emssanar_new/pages/sections/submenuriastecnologias.dart';
import 'package:emssanar_new/pages/sections/redUrgencias.dart';
import 'package:emssanar_new/pages/tecnologiaspage.dart';
//import 'package:emssanar_new/models/InteraccionCovidModel.dart';
import 'package:qlevar_router/qlevar_router.dart';

import 'package:emssanar_new/pages/actualizarpacientepage.dart';
import 'package:emssanar_new/pages/datosapppage.dart';
import 'package:emssanar_new/pages/loginpage.dart';
import 'package:emssanar_new/pages/indexpage.dart';
import 'package:emssanar_new/pages/homepage.dart';
import 'package:emssanar_new/pages/interaccioncovidpage.dart';
//import 'helpers/database.dart';
/* import 'helpers/text_page.dart';
import 'screens/home_page.dart';
import 'screens/nested2.dart';
import 'screens/nested_route.dart';
import 'screens/parent_page.dart'; */
//import 'screens/sitemap_page.dart';

class AppRoutes {
  /* static const nested = 'Nested';
  static const nestedChild = 'Nested Child';
  static const nestedChild1 = 'Nested Child 1';
  static const nestedChild2 = 'Nested Child 2';
  static const nestedChild3 = 'Nested Child 3'; */

  ///
  static const applicacion = 'Appicacion';
  static const inicio = 'Inicio';
  static const login = 'Login';
  static const lista = 'Lista';
  /* static const app = 'App';
  static const home = 'Home';
  static const settings = 'Settings'; */

  List<QRoute> routes() => [
        QRoute(path: '/', builder: () => IndexPage()),
        //QRoute(path: '/', builder: () => ActualizarPaciente()),
        QRoute(path: '/homeRuta', builder: () => HomePage()),
        QRoute(path: '/loginRuta', builder: () => LoginPage()),
        QRoute(path: '/datosappgraphRuta', builder: () => DatosAppPage()),
        QRoute(path: '/verpaginaRuta', builder: () => VerPagina()),
        QRoute(path: '/actpacienteRuta', builder: () => ActualizarPacientePage()),

        QRoute(
            path: '/interaccioncovidRuta',
            builder: () => InteraccionCovidPage()),
        QRoute(
            path: '/interaccionappRuta', builder: () => InteraccionAppPage()),
        QRoute(path: '/redUrgenciasMapa', builder: () => RedUrgenciasMapa()),
        QRoute(path: '/citasRuta', builder: () => CitasPage()),
        QRoute(path: '/tecnologiasRuta', builder: () => TecnologiasPage()),
        QRoute(path: '/eventosRuta', builder: () => EventosPage()),
        QRoute(path: '/submenuEstandarRuta', builder: () => SubMenuEstandar()),
        QRoute(
            path: '/prestadorespacienteRuta',
            builder: () => PrestadoresPacientePage()),
        QRoute(path: '/rutasatencionRuta', builder: () => RutasAtencionPage()),
        //QRoute(path: '/submenurutasatencionRuta', builder: () => RutasAtencionPage()),
        //menuRutasAtencion
        QRoute(
            path: '/menuactualizarinfoRuta',
            builder: () => MenuActuaizarInfoPersonal()),
        QRoute(path: '/menuredserviciosRuta', builder: () => RedDeServicios()),
        QRoute(
            path: '/menuriastecnoligiasRuta', builder: () => RiasTecnologias()),
        QRoute(path: '/menueventosRuta', builder: () => MenuEventos()),
        QRoute(path: '/estadoAutorizaciones', builder: () => EstadoAutorizaciones()),

        /*   QRoute(
            path: '/parent',
            builder: () {
              print('-- Build Parent page --');
              return ParentPage();
            },
            middleware: [
              QMiddlewareBuilder(
                  onEnterFunc: () => print('-- Enter Parent page --'),
                  onExitFunc: () => print('-- Exit Parent page --'),
                  onMatchFunc: () => print('-- Parent page Matched --'))
            ],
            children: [
              QRoute(path: '/child', builder: () => TextPage('Hi child')),
              QRoute(path: '/child-1', builder: () => TextPage('Hi child 1')),
              QRoute(path: '/child-2', builder: () => TextPage('Hi child 2')),
              QRoute(path: '/child-3', builder: () => TextPage('Hi child 3')),
              QRoute(
                  path: '/child-4',
                  middleware: [
                    QMiddlewareBuilder(
                        redirectGuardFunc: (s) => Future.delayed(
                              Duration(milliseconds: 100),
                              /* () => Database.canChildNavigate
                                ? null
                                : '/parent/child-2' */
                            ))
                  ],
                  builder: () => TextPage('Hi child 4')),
            ]),
        QRoute(
            path: '/:id',
            builder: () => TextPage('the id is ${QR.params['id']}')),
        QRoute(path: '/params', builder: () => TextPage(
            // ignore: lines_longer_than_80_chars
            'params are: test is${QR.params['test']} and go is ${QR.params['go']}')),
        /* QRoute(path: '/sitemap', builder: () => SiteMap()), */
        QRoute.withChild(
            name: nested,
            path: '/nested',
            builderChild: (r) => NestedRoutePage(r),
            initRoute: '/child',
            children: [
              QRoute(
                  name: nestedChild,
                  path: '/child',
                  builder: () => NestedChild('child')),
              QRoute(
                  name: nestedChild1,
                  path: '/child-1',
                  builder: () => NestedChild('child 1'),
                  pageType: QSlidePage()),
              QRoute(
                  name: nestedChild2,
                  path: '/child-2',
                  builder: () => NestedChild('child 2')),
              QRoute(
                  name: nestedChild3,
                  path: '/child-3',
                  builder: () => NestedChild('child 3')),
            ]),
        QRoute(path: '/loginRuta', builder: () => LoginScreen()),
        //QRoute(path: '/homeRuta', builder: () => HomePage()),
        QRoute.withChild(
            name: app,
            path: '/app',
            builderChild: (child) => AppScreen(child),
            initRoute: '/home',
            children: [
              QRoute(name: home, path: '/home', builder: () => HomeWidget()),
              QRoute(
                  name: settings,
                  path: '/settings',
                  builder: () => SettingsWidget(),
                  pageType: QSlidePage()),
            ]),
       */
      ];
}
