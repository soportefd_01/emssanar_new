/* class APPMENU {
  Data data;

  APPMENU({this.data});

  APPMENU.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Datosapp> datosapp;

  Data({this.datosapp});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['datosapp'] != null) {
      datosapp = new List<Datosapp>();
      json['datosapp'].forEach((v) {
        datosapp.add(new Datosapp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.datosapp != null) {
      data['datosapp'] = this.datosapp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Datosapp {
  String titulo;
  String descripcion;
  String imagen;
  String tipo;
  int orden;
  String usuario;
  String menu;

  Datosapp(
      {this.titulo,
      this.descripcion,
      this.imagen,
      this.tipo,
      this.orden,
      this.usuario,
      this.menu});

  Datosapp.fromJson(Map<String, dynamic> json) {
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    imagen = json['imagen'];
    tipo = json['tipo'];
    orden = json['orden'];
    usuario = json['usuario'];
    menu = json['menu'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['imagen'] = this.imagen;
    data['tipo'] = this.tipo;
    data['orden'] = this.orden;
    data['usuario'] = this.usuario;
    data['menu'] = this.menu;
    return data;
  }
}
 */

/* // */
