/* class CONTACTOSPRESTADOR {
  Data data;

  CONTACTOSPRESTADOR({this.data});

  CONTACTOSPRESTADOR.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<ContactosIps> contactosIps;

  Data({this.contactosIps});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['contactosIps'] != null) {
      contactosIps = new List<ContactosIps>();
      json['contactosIps'].forEach((v) {
        contactosIps.add(new ContactosIps.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.contactosIps != null) {
      data['contactosIps'] = this.contactosIps.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ContactosIps {
  String ips;
  String tipo;
  String contacto;
  String observacion;

  ContactosIps({this.ips, this.tipo, this.contacto, this.observacion});

  ContactosIps.fromJson(Map<String, dynamic> json) {
    ips = json['ips'];
    tipo = json['tipo'];
    contacto = json['contacto'];
    observacion = json['observacion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ips'] = this.ips;
    data['tipo'] = this.tipo;
    data['contacto'] = this.contacto;
    data['observacion'] = this.observacion;
    return data;
  }
}
 */