/* class EVENTOS {
  Data data;

  EVENTOS({this.data});

  EVENTOS.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<EventosEps> eventosEps;

  Data({this.eventosEps});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['eventosEps'] != null) {
      eventosEps = new List<EventosEps>();
      json['eventosEps'].forEach((v) {
        eventosEps.add(new EventosEps.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.eventosEps != null) {
      data['eventosEps'] = this.eventosEps.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class EventosEps {
  String titulo;
  String departamento;
  String municipio;
  String descripcion;
  String lugar;
  String fecha;
  String horaInicio;
  String horaFin;
  String imagen;

  EventosEps(
      {this.titulo,
      this.departamento,
      this.municipio,
      this.descripcion,
      this.lugar,
      this.fecha,
      this.horaInicio,
      this.horaFin,
      this.imagen});

  EventosEps.fromJson(Map<String, dynamic> json) {
    titulo = json['titulo'];
    departamento = json['departamento'];
    municipio = json['municipio'];
    descripcion = json['descripcion'];
    lugar = json['lugar'];
    fecha = json['fecha'];
    horaInicio = json['horaInicio'];
    horaFin = json['horaFin'];
    imagen = json['imagen'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['titulo'] = this.titulo;
    data['departamento'] = this.departamento;
    data['municipio'] = this.municipio;
    data['descripcion'] = this.descripcion;
    data['lugar'] = this.lugar;
    data['fecha'] = this.fecha;
    data['horaInicio'] = this.horaInicio;
    data['horaFin'] = this.horaFin;
    data['imagen'] = this.imagen;
    return data;
  }
}
 */