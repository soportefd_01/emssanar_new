/* class PRESTADORES {
  Data data;

  PRESTADORES({this.data});

  PRESTADORES.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<PrestadoresPaciente> prestadoresPaciente;

  Data({this.prestadoresPaciente});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['prestadoresPaciente'] != null) {
      prestadoresPaciente = new List<PrestadoresPaciente>();
      json['prestadoresPaciente'].forEach((v) {
        prestadoresPaciente.add(new PrestadoresPaciente.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.prestadoresPaciente != null) {
      data['prestadoresPaciente'] =
          this.prestadoresPaciente.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PrestadoresPaciente {
  String identificacion;
  String prestadorPrimario;
  String medicinaGeneral;
  String odontologia;
  String pyp;
  String medicamentos;

  PrestadoresPaciente(
      {this.identificacion,
      this.prestadorPrimario,
      this.medicinaGeneral,
      this.odontologia,
      this.pyp,
      this.medicamentos});

  PrestadoresPaciente.fromJson(Map<String, dynamic> json) {
    identificacion = json['identificacion'];
    prestadorPrimario = json['prestadorPrimario'];
    medicinaGeneral = json['medicinaGeneral'];
    odontologia = json['odontologia'];
    pyp = json['pyp'];
    medicamentos = json['medicamentos'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identificacion'] = this.identificacion;
    data['prestadorPrimario'] = this.prestadorPrimario;
    data['medicinaGeneral'] = this.medicinaGeneral;
    data['odontologia'] = this.odontologia;
    data['pyp'] = this.pyp;
    data['medicamentos'] = this.medicamentos;
    return data;
  }
}
 */