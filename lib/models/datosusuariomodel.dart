class CONSUTLARCONTACTO {
  Data data;

  CONSUTLARCONTACTO({this.data});

  CONSUTLARCONTACTO.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  ContactosporId contactosporId;

  Data({this.contactosporId});

  Data.fromJson(Map<String, dynamic> json) {
    contactosporId = json['contactosporId'] != null
        ? new ContactosporId.fromJson(json['contactosporId'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.contactosporId != null) {
      data['contactosporId'] = this.contactosporId.toJson();
    }
    return data;
  }
}

class ContactosporId {
  int id;
  String tipoIdentificacion;
  String identificacion;
  String nombre;
  String telefono;
  String celular;
  String email;
  String direccion;
  String ips;
  String municipio;

  ContactosporId({
    this.id,
    this.tipoIdentificacion,
    this.identificacion,
    this.nombre,
    this.telefono,
    this.celular,
    this.email,
    this.direccion,
    this.ips,
    this.municipio,
  });

  ContactosporId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipoIdentificacion = json['tipoIdentificacion'];
    identificacion = json['identificacion'];
    nombre = json['nombre'];
    telefono = json['telefono'];
    celular = json['celular'];
    email = json['email'];
    direccion = json['direccion'];
    ips = json['ips'];
    municipio = json['municipio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipoIdentificacion'] = this.tipoIdentificacion;
    data['identificacion'] = this.identificacion;
    data['nombre'] = this.nombre;
    data['telefono'] = this.telefono;
    data['celular'] = this.celular;
    data['email'] = this.email;
    data['direccion'] = this.direccion;
    data['ips'] = this.ips;
    data['municipio'] = this.municipio;
    return data;
  }
}

/* // */
