/* class RUTAS {
  Data data;

  RUTAS({this.data});

  RUTAS.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<RutasPaciente> rutasPaciente;

  Data({this.rutasPaciente});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['rutasPaciente'] != null) {
      rutasPaciente = new List<RutasPaciente>();
      json['rutasPaciente'].forEach((v) {
        rutasPaciente.add(new RutasPaciente.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.rutasPaciente != null) {
      data['rutasPaciente'] =
          this.rutasPaciente.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RutasPaciente {
  String identificacion;
  String ruta;
  String procedimiento;
  String ips;
  String cumplimiento;
  String fecha;

  RutasPaciente(
      {this.identificacion,
      this.ruta,
      this.procedimiento,
      this.ips,
      this.cumplimiento,
      this.fecha});

  RutasPaciente.fromJson(Map<String, dynamic> json) {
    identificacion = json['identificacion'];
    ruta = json['ruta'];
    procedimiento = json['procedimiento'];
    ips = json['ips'];
    cumplimiento = json['cumplimiento'];
    fecha = json['fecha'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identificacion'] = this.identificacion;
    data['ruta'] = this.ruta;
    data['procedimiento'] = this.procedimiento;
    data['ips'] = this.ips;
    data['cumplimiento'] = this.cumplimiento;
    data['fecha'] = this.fecha;
    return data;
  }
}
 */