class INTERACCIONAPP {
  Data data;

  INTERACCIONAPP({this.data});

  INTERACCIONAPP.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  CrearInteraccionAPP crearInteraccionAPP;

  Data({this.crearInteraccionAPP});

  Data.fromJson(Map<String, dynamic> json) {
    crearInteraccionAPP = json['crearInteraccionAPP'] != null
        ? new CrearInteraccionAPP.fromJson(json['crearInteraccionAPP'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.crearInteraccionAPP != null) {
      data['crearInteraccionAPP'] = this.crearInteraccionAPP.toJson();
    }
    return data;
  }
}

class CrearInteraccionAPP {
  Interaccion interaccion;

  CrearInteraccionAPP({this.interaccion});

  CrearInteraccionAPP.fromJson(Map<String, dynamic> json) {
    interaccion = json['interaccion'] != null
        ? new Interaccion.fromJson(json['interaccion'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.interaccion != null) {
      data['interaccion'] = this.interaccion.toJson();
    }
    return data;
  }
}

class Interaccion {
  int id;
  String identificacion;
  String descripcion;
  String tipoInteraccion;
  String clasificacion;
  String prioridad;
  String medioRecepcion;
  String personaRegistro;

  Interaccion(
      {this.id,
      this.identificacion,
      this.descripcion,
      this.tipoInteraccion,
      this.clasificacion,
      this.prioridad,
      this.medioRecepcion,
      this.personaRegistro});

  Interaccion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    identificacion = json['identificacion'];
    descripcion = json['descripcion'];
    tipoInteraccion = json['tipoInteraccion'];
    clasificacion = json['clasificacion'];
    prioridad = json['prioridad'];
    medioRecepcion = json['medioRecepcion'];
    personaRegistro = json['personaRegistro'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['identificacion'] = this.identificacion;
    data['descripcion'] = this.descripcion;
    data['tipoInteraccion'] = this.tipoInteraccion;
    data['clasificacion'] = this.clasificacion;
    data['prioridad'] = this.prioridad;
    data['medioRecepcion'] = this.medioRecepcion;
    data['personaRegistro'] = this.personaRegistro;
    return data;
  }
}
