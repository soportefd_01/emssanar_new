import 'package:flutter/material.dart';

class RedProvider with ChangeNotifier {
  String _municipio = "";
  int _tipoRed = -1;
  String _prestador = "";
  List _contactos = [];


  String get municipio => _municipio;


  set municipio(String value) {
    _municipio = value;
    notifyListeners();
  }

  int get tipoRed => _tipoRed;

  String get prestador => _prestador;

  set prestador(String value) {
    _prestador = value;
  }

  set tipoRed(int value) {
    _tipoRed = value;
    notifyListeners();
  }

  List get contactos => _contactos;

  set contactos(List value) {
    _contactos = value;
    notifyListeners();
  }
}
