//en caso de descomentar hacerlo hasta la ultima linea‹‹‹‹‹

//de aqui para arriba funciona bien, se comenta solo para meter en un proividers y manejo de estaods
import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/validacioncampo.dart';

class InteraccionCovidProvider with ChangeNotifier {
  bool _aceptavacunacionChange = false;
  bool _vacunadoChange = false;
  bool _programadoIpsChange = false;

  ValidacionCampo _identificacionval = ValidacionCampo(null, null, false);
  ValidacionCampo _motivoRechazo = ValidacionCampo(null, null, false);
  ValidacionCampo _acudiente = ValidacionCampo(null, null, false);
  ValidacionCampo _telefonoAcudiente = ValidacionCampo(null, null, false);
  ValidacionCampo _reconfirmarMunicipio = ValidacionCampo(null, null, false);
  ValidacionCampo _descripcion = ValidacionCampo(null, null, false);
//Getters
  ValidacionCampo get getIdentificacion => _identificacionval;
  ValidacionCampo get getMotivoRechazo => _motivoRechazo;
  ValidacionCampo get getAcudiente => _acudiente;
  ValidacionCampo get getTelefonoAcudiente => _telefonoAcudiente;
  ValidacionCampo get getReconfirmarMunicipio => _reconfirmarMunicipio;
  ValidacionCampo get getDescripcion => _descripcion;
  get getAceptavacunacionChange => _aceptavacunacionChange;
  get getVacunadoChange => _vacunadoChange;
  get getProgramadoIpsChange => _programadoIpsChange;

  bool get isValid {
    if (_acudiente.valor != null &&
        _acudiente.valor.length > 1 &&
        _telefonoAcudiente.valor != null &&
        _telefonoAcudiente.valor.length > 1 &&
        _reconfirmarMunicipio.valor != null &&
        _reconfirmarMunicipio.valor.length > 1 &&
        _descripcion.valor != null &&
        _descripcion.valor.length > 1) {
      return true;
    } else {
      return false;
    }
  }

//setters
  void changeIdentificacion(String value) {
    if (value.length > 1 && value.length <= 10) {
      _identificacionval = ValidacionCampo(value, null, true);
    } else {
      _identificacionval =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

  void changeMotivoRechazo(String value) {
    if (value.length <= 10) {
      _motivoRechazo = ValidacionCampo(value, null, true);
    } else {
      _motivoRechazo =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

  void changeAcudiente(String value) {
    if (value.length > 1 && value.length <= 10) {
      _acudiente = ValidacionCampo(value, null, true);
    } else {
      _acudiente =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

  void changeTelefonoAcudiente(String value) {
    if (value.length > 1 && value.length <= 10) {
      _telefonoAcudiente = ValidacionCampo(value, null, true);
    } else {
      _telefonoAcudiente =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

  void changeReconfirmarMunicipio(String value) {
    if (value.length > 1 && value.length <= 10) {
      _reconfirmarMunicipio = ValidacionCampo(value, null, true);
    } else {
      _reconfirmarMunicipio =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

  void changeDescripcion(String value) {
    if (value.length > 1 && value.length <= 10) {
      _descripcion = ValidacionCampo(value, null, true);
    } else {
      _descripcion =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca", false);
    }
    notifyListeners();
  }

//checkbox
  set setAceptavacunacionChange(bool aceptavacunacionChange) {
    this._aceptavacunacionChange = aceptavacunacionChange;
    notifyListeners();
  }

  set setVacunadoChange(bool vacunadoChange) {
    this._vacunadoChange = vacunadoChange;
    notifyListeners();
  }

  set setProgramadoIpsChange(bool programadoIpsChange) {
    this._programadoIpsChange = programadoIpsChange;
    notifyListeners();
  }

  void submitData() {
    print(
        "faltan AGREGAR CAMPOS FirstName: ${getIdentificacion.valor}, LastName: ${getMotivoRechazo.valor},${getAcudiente.valor}");
  }

//de aqui para arriba funciona bien, se comenta solo para meter en un proividers y manejo de estaods

  /* ValidationItem _firstName = ValidationItem(null, null,false);
  ValidationItem _lastName = ValidationItem(null, null,false);
  ValidationItem _dob = ValidationItem(null, null,false); */
  //ValidacionCampoBool _programadoIpsBool = ValidacionCampoBool(null, null,false);
  //ValidacionCampo _aceptavacunacion = ValidacionCampo(null, null,false);
  //ValidacionCampo _vacunado = ValidacionCampo(null, null,false);
  //ValidacionCampo _programadoIps = ValidacionCampo(null, null,false);

  //ValidacionCampoBool get getProgramadoIps => _programadoIpsBool;
  //ValidacionCampo get getVacunacion => _aceptavacunacion;
  //ValidacionCampo get getVacunado => _vacunado;
  //ValidacionCampo get getProgramadoIps => _programadoIps;

//Getters
  /* ValidationItem get firstName => _firstName;
  ValidationItem get lastName => _lastName;
  ValidationItem get dob => _dob; */
  /* bool get isValid {
    if (_lastName.value != null &&
        _firstName.value != null &&
        dob.value != null) {
      return true;
    } else {
      return false;
    }
  } */

  /* void changeVacunacion(String value) {
    if (value.length > 1 && value.length <= 10) {
      _aceptavacunacion = ValidacionCampo(value, null,true);
    } else {
      _aceptavacunacion =
          ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca",false);
    }
    notifyListeners();
  } */

  /* void changeVacunado(String value) {
    if (value.length > 1 && value.length <= 10) {
      _vacunado = ValidacionCampo(value, null,true);
    } else {
      _vacunado = ValidacionCampo(value, "Campo obligatorio entre 1 y 10 ca",false);
    }
    notifyListeners();
  } */

  /* void changeProgramadoIps(bool value) {
    if (value || !value) {
      _programadoIpsBool = ValidacionCampoBool(value, null);
    } else {
      _programadoIpsBool =
          ValidacionCampoBool(value, "Campo obligatorio entre 1 y 10 ca",false);
    }
    notifyListeners();
  } */

//Setters
  /* void changeFirstName(String value) {
    if (value.length >= 3) {
      _firstName = ValidationItem(value, null);
    } else if (value.length <= 1) {
      _firstName = ValidationItem(null, "Debe tener más de un caracter");
    } else {
      _firstName = ValidationItem(null, "ok");
    }
    notifyListeners();
  }

  void changeLastName(String value) {
    if (value.length >= 3) {
      _lastName = ValidationItem(value, null);
    } else {
      _lastName = ValidationItem(null, "Must be at least 3 characters");
    }
    notifyListeners();
  }

  void changeDOB(String value) {
    try {
      DateTime.parse(value);
      _dob = ValidationItem(value, null);
    } catch (error) {
      _dob = ValidationItem(null, "Invalid Format");
    }
    notifyListeners();
  } */

  /* void submitData() {
    print(
        "FirstName: ${firstName.value}, LastName: ${lastName.value},${DateTime.parse(dob.value)}");
  } */

}
