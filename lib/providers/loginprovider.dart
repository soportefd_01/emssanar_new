import 'package:flutter/material.dart';
import 'package:crypto/crypto.dart'; //PARA ENCRYPTAR LA CONSTRASEÑA
import 'dart:convert'; //PARA CONVERTIR ENCODE..DECODE

class LoginProvider with ChangeNotifier {
  String _tipoidentificacion = ""; //tipo identificación
  String _identificacion = ""; //identicación
  int _id; //codigo unico en la base de datos PK
  String _usuario = "USUARIO GENERICO";
  String _contrasena = "";
  bool _loading = false;
  bool _estaAutenticado = false; //valida si esta o no autenticado
  /* String _dia = "";
  String _mes = "";
  String _anio = ""; */

  get getTipoIdentificacion {
    return _tipoidentificacion;
  }

  get getIdentificacion {
    return _identificacion;
  }

  get getId {
    return _id;
  }

  get getUsuario {
    return _usuario;
  }

  get getContrasena {
    return _contrasena;
  }

  get getLoading {
    return _loading;
  }

  get getEstaAutenticado {
    return _estaAutenticado;
  }

  /* get getDia {
    return _dia;
  }

  get getMes {
    return _mes;
  }

  get getAnio {
    return _anio;
  } */

  set setTipoIdentificacion(String tipoidentificacion) {
    this._tipoidentificacion = tipoidentificacion;
    notifyListeners();
  }

  set setIdentificacion(String identificacion) {
    this._identificacion = identificacion;
    notifyListeners();
  }

  set setId(int id) {
    this._id = id;
    notifyListeners();
  }

  set setUsuario(String usuario) {
    this._usuario = usuario;
    notifyListeners();
  }

  //ENCRIPTA LA CONTRASEÑA CON SHA512
  set setContrasena(String contrasena) {
    /* var bytes = utf8.encode(contrasena); // data being hashed
    var digest = sha512.convert(bytes);
    this._contrasena = digest.toString(); */
    this._contrasena = contrasena;
    notifyListeners();
  }

  set setLoading(bool loading) {
    this._loading = loading;
    notifyListeners();
  }

  set setEstaAutenticado(bool estaautenticado) {
    this._estaAutenticado = estaautenticado;
    notifyListeners();
  }

/*   set setDia(String dia) {
    this._dia = dia;
    notifyListeners();
  }

  set setMes(String mes) {
    this._mes = mes;
    notifyListeners();
  }

  set setAnio(String anio) {
    this._anio = anio;
    notifyListeners();
  } */
}
