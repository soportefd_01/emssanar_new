/* src/validation/signup_validation.dart */

import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/validacioncampo.dart';

class InteraccionAppProvider with ChangeNotifier {
  //ValidacionCampo _identificacion = ValidacionCampo(null, null, false);
  ValidacionCampo _descripcion = ValidacionCampo(null, null, false);

//Getters
  //ValidacionCampo get getIdentificacion => _identificacion;
  ValidacionCampo get getDescripcion => _descripcion;

  bool get isValid {
    if (_descripcion.valor != null && _descripcion.valor.length >= 5) {
      return true;
    } else {
      return false;
    }
  }

//setters
  /* void changeIdentificacion(String value) {
    if (value.length > 1) {
      _identificacion = ValidacionCampo(value, null, true);
    } else {
      _identificacion = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  } */

  void changeDescripcion(String value) {
    if (value.length >= 5) {
      _descripcion = ValidacionCampo(value, null, true);
    } else {
      _descripcion = ValidacionCampo(
          value, "Campo obligatorio mayor a 5 caracteres", false);
    }
    notifyListeners();
  }

  void submitData() {
    print(
        "faltan AGREGAR CAMPOS FirstName:  descripcion: ${getDescripcion.valor},");
  }
}
