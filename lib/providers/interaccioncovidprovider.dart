/* src/validation/signup_validation.dart */

import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/validacioncampo.dart';

class InteraccionCovidProvider with ChangeNotifier {
  bool _aceptavacunacionChange = false;
  bool _vacunadoChange = false;
  bool _programadoIpsChange = false;

  /* ValidationItem _firstName = ValidationItem(null, null);
  ValidationItem _lastName = ValidationItem(null, null);
  ValidationItem _dob = ValidationItem(null, null); */

  ValidacionCampo _identificacion = ValidacionCampo(null, null, false);
  ValidacionCampo _motivoRechazo = ValidacionCampo(null, null, false);
  ValidacionCampo _acudiente = ValidacionCampo(null, null, false);
  ValidacionCampo _telefonoAcudiente = ValidacionCampo(null, null, false);
  ValidacionCampo _reconfirmarMunicipio = ValidacionCampo(null, null, false);
  ValidacionCampo _descripcion = ValidacionCampo(null, null, false);
  //ValidacionCampoBool _programadoIpsBool = ValidacionCampoBool(null, null);
  //ValidacionCampo _aceptavacunacion = ValidacionCampo(null, null);
  //ValidacionCampo _vacunado = ValidacionCampo(null, null);
  //ValidacionCampo _programadoIps = ValidacionCampo(null, null);

//Getters
  ValidacionCampo get getIdentificacion => _identificacion;
  ValidacionCampo get getMotivoRechazo => _motivoRechazo;
  ValidacionCampo get getAcudiente => _acudiente;
  ValidacionCampo get getTelefonoAcudiente => _telefonoAcudiente;
  ValidacionCampo get getReconfirmarMunicipio => _reconfirmarMunicipio;
  ValidacionCampo get getDescripcion => _descripcion;
  get getAceptavacunacionChange => _aceptavacunacionChange;
  get getVacunadoChange => _vacunadoChange;
  get getProgramadoIpsChange => _programadoIpsChange;

  //ValidacionCampoBool get getProgramadoIps => _programadoIpsBool;
  //ValidacionCampo get getVacunacion => _aceptavacunacion;
  //ValidacionCampo get getVacunado => _vacunado;
  //ValidacionCampo get getProgramadoIps => _programadoIps;

//Getters
  /* ValidationItem get firstName => _firstName;
  ValidationItem get lastName => _lastName;
  ValidationItem get dob => _dob; */
  /* bool get isValid {
    if (_lastName.value != null &&
        _firstName.value != null &&
        dob.value != null) {
      return true;
    } else {
      return false;
    }
  } */

  bool get isValid {
    if (_acudiente.valor != null &&
        _acudiente.valor.length > 3 &&
        _telefonoAcudiente.valor != null &&
        _telefonoAcudiente.valor.length > 3 &&
        _reconfirmarMunicipio.valor != null &&
        _reconfirmarMunicipio.valor.length > 3 &&
        _descripcion.valor != null &&
        _descripcion.valor.length > 3) {
      return true;
    } else {
      return false;
    }
  }

//setters
  void changeIdentificacion(String value) {
    if (value.length > 1) {
      _identificacion = ValidacionCampo(value, null, true);
    } else {
      _identificacion = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void changeMotivoRechazo(String value) {
    if (value.length <= 10) {
      _motivoRechazo = ValidacionCampo(value, null, true);
    } else {
      _motivoRechazo = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void changeAcudiente(String value) {
    if (value.length > 3) {
      _acudiente = ValidacionCampo(value, null, true);
    } else {
      _acudiente = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void changeTelefonoAcudiente(String value) {
    if (value.length > 3) {
      _telefonoAcudiente = ValidacionCampo(value, null, true);
    } else {
      _telefonoAcudiente = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void changeReconfirmarMunicipio(String value) {
    if (value.length > 3) {
      _reconfirmarMunicipio = ValidacionCampo(value, null, true);
    } else {
      _reconfirmarMunicipio = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void changeDescripcion(String value) {
    if (value.length > 5) {
      _descripcion = ValidacionCampo(value, null, true);
    } else {
      _descripcion = ValidacionCampo(
          value, "Campo obligatorio mayor a 5 caracteres", false);
    }
    notifyListeners();
  }

//solo se asigna el valor, + no se valida nada
//checkbox
  set setAceptavacunacionChange(bool aceptavacunacionChange) {
    this._aceptavacunacionChange = aceptavacunacionChange;
    notifyListeners();
  }

  set setVacunadoChange(bool vacunadoChange) {
    this._vacunadoChange = vacunadoChange;
    notifyListeners();
  }

  set setProgramadoIpsChange(bool programadoIpsChange) {
    this._programadoIpsChange = programadoIpsChange;
    notifyListeners();
  }

  void submitData() {
    print(
        "faltan AGREGAR CAMPOS FirstName: ${getIdentificacion.valor}, LastName: ${getMotivoRechazo.valor},${getAcudiente.valor}");
  }

  //se valida que solamente sea true o false desde la interfaz,
  //para ellos es necesario habilitar la clase ValidacionCampoBool
  //en helpers vadlidacionCampo.dart
  /* void changeProgramadoIps(bool value) {
    if (value || !value) {
      _programadoIpsBool = ValidacionCampoBool(value, null);
    } else {
      _programadoIpsBool =
          ValidacionCampoBool(value, "Campo obligatorio mayor a 3 caracteres",false);
    }
    notifyListeners();
  } */

  /* void changeVacunacion(String value) {
    if (value.length > 1 ) {
      _aceptavacunacion = ValidacionCampo(value, null,true);
    } else {
      _aceptavacunacion =
          ValidacionCampo(value, "Campo obligatorio mayor a 3 caracteres",false);
    }
    notifyListeners();
  } */

  /* void changeVacunado(String value) {
    if (value.length > 1 ) {
      _vacunado = ValidacionCampo(value, null,true);
    } else {
      _vacunado = ValidacionCampo(value, "Campo obligatorio mayor a 3 caracteres",false);
    }
    notifyListeners();
  } */

//Setters
  /* void changeFirstName(String value) {
    if (value.length >= 3) {
      _firstName = ValidationItem(value, null);
    } else if (value.length <= 1) {
      _firstName = ValidationItem(null, "Debe tener más de un caracter");
    } else {
      _firstName = ValidationItem(null, "ok");
    }
    notifyListeners();
  }

  void changeLastName(String value) {
    if (value.length >= 3) {
      _lastName = ValidationItem(value, null);
    } else {
      _lastName = ValidationItem(null, "Must be at least 3 characters");
    }
    notifyListeners();
  }

  void changeDOB(String value) {
    try {
      DateTime.parse(value);
      _dob = ValidationItem(value, null);
    } catch (error) {
      _dob = ValidationItem(null, "Invalid Format");
    }
    notifyListeners();
  } */

  /* void submitData() {
    print(
        "FirstName: ${firstName.value}, LastName: ${lastName.value},${DateTime.parse(dob.value)}");
  } */

}
