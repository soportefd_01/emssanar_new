import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/validacioncampo.dart';
import 'package:geolocator/geolocator.dart'; //para validaciones

class ContactosProvider with ChangeNotifier {
  //ESTADOS
  //declaracion de varibales para estados
  int _id = 0;
  String _tipoIdentificacion = " ";
  String _identificacion = " ";
  String _nombre = " ";
  String _telefono = " ";
  String _celular = " ";
  String _email = " ";
  String _direccion = " ";
  String _ips = " ";
  String _municipio = " ";
  bool _ubicacion = false;
  bool _cargandoUbicacion = false;

  //VALIDACIONES
  //declaracion de variables para validaciones
  ValidacionCampo _identificacionval = ValidacionCampo(null, null, false);
  ValidacionCampo _telefonoval = ValidacionCampo(null, null, false);
  ValidacionCampo _celularval = ValidacionCampo(null, null, false);
  ValidacionCampo _correoval = ValidacionCampo(null, null, false);
  ValidacionCampo _direccionval = ValidacionCampo(null, null, false);

  //ESTADOS
  //Getter para estados
  get getId => _id;
  get getTipoIdentificacion => _tipoIdentificacion;
  get getIdentificacion => _identificacion;
  get getNombre => _nombre;
  get getTelefono => _telefono;
  get getCelular => _celular;
  get getEmail => _email;
  get getDireccion => _direccion;
  get getIps => _ips;

  bool get cargandoUbicacion => _cargandoUbicacion;

  get getMunicipio => _municipio;

  bool get ubicacion => _ubicacion; //VALIDACIONES
  //Getters para validaciones campos de texto y checks
  ValidacionCampo get getIdentificacionVal => _identificacionval;
  ValidacionCampo get getTelefonoVal => _telefonoval;
  ValidacionCampo get getCelularVal => _celularval;
  ValidacionCampo get getCorreoVal => _correoval;
  ValidacionCampo get getDireccionVal => _direccionval;

  //ESTADOS
  //Setter para asignar a estados sin validaciones
  set setId(int id) {
    this._id = id;
    notifyListeners();
  }


  set cargandoUbicacion(bool value) {
    _cargandoUbicacion = value;
    notifyListeners();
  }

  set ubicacion(bool val) {
    if(val){
      this.cargandoUbicacion = true;
      _determinePosition().then((value) {
        this.cargandoUbicacion = false;
        _ubicacion = val;
        notifyListeners();
      }).whenComplete(() {
        this.cargandoUbicacion = false;
      });
    } else {
      _ubicacion = val;
      notifyListeners();
    }
  }

  set setTipoIdentificacion(String tipoIdentificacion) {
    this._tipoIdentificacion = tipoIdentificacion;
    notifyListeners();
  }

  set setIdentificacion(String identificacion) {
    this._identificacion = identificacion;
    notifyListeners();
  }

  set setNombre(String nombre) {
    this._nombre = nombre;
    notifyListeners();
  }

  set setTelefono(String telefono) {
    this._telefono = telefono;
    notifyListeners();
  }

  set setCelular(String celular) {
    this._celular = celular;
    notifyListeners();
  }

  set setEmail(String email) {
    this._email = email;
    notifyListeners();
  }

  set setDireccion(String direccion) {
    this._direccion = direccion;
    notifyListeners();
  }

  set setIps(String ips) {
    this._ips = ips;
    notifyListeners();
  }

  set setMunicipio(String municipio) {
    this._municipio = municipio;
    notifyListeners();
  }

//VALIDACIONES
//función que valida si los campos tienen más de un caracter
//o si estan vacios

  bool get isValid {
    if (_telefonoval.valor != null &&
        _telefonoval.valor.length == 7 &&
        _celularval.valor != null &&
        _celularval.valor.length == 10 &&
        _correoval.valor != null &&
        _direccionval.valor != null &&
        _direccionval.valor.length > 3) {
      return true;
    } else {
      return false;
    }
  }

//VALIDACIONES
//setters validaciones para mostrar el error en hint error de camda campo de texto
  void identificacionValidacion(String value) {
    if (value.length > 3) {
      _identificacionval = ValidacionCampo(value, null, true);
    } else {
      _identificacionval = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  void telefonoValidacion(String value) {
    if (value.length == 7) {
      _telefonoval = ValidacionCampo(value, null, true);
    } else {
      _telefonoval =
          ValidacionCampo(value, "Campo obligatorio igual a 7 digitos", false);
    }
    notifyListeners();
  }

  void celularValidacion(String value) {
    if (value.length == 10) {
      _celularval = ValidacionCampo(value, null, true);
    } else {
      _celularval = ValidacionCampo(
          value, "Campo obligatorio mayor o igual a 10 dígitos", false);
    }
    notifyListeners();
  }

  void correoValidacion(String value) {
    if (validateEmail(value)) {
      _correoval = ValidacionCampo(value, null, true);
    } else {
      _correoval = ValidacionCampo(value, "Correo no valido", false);
    }
    notifyListeners();
  }

  void direccionValidacion(String value) {
    if (value.length > 3) {
      _direccionval = ValidacionCampo(value, null, true);
    } else {
      _direccionval = ValidacionCampo(
          value, "Campo obligatorio mayor a 3 caracteres", false);
    }
    notifyListeners();
  }

  //no se usa solo es para documentación
  void submitData() {
    print(
        "faltan AGREGAR CAMPOS FirstName: ${getIdentificacionVal.valor}, LastName: ${getTelefonoVal.valor},${getCelularVal.valor}");
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }
}

Future<Position> _determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}
