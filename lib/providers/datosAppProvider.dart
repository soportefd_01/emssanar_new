import 'package:flutter/material.dart';

class DatosAppProvider with ChangeNotifier {
  String _titulo = "";
  String _descripcion = "";
  String _imagen = "";
  String _tipo = "";
  int _orden = 0;
  String _usuario = "";
  String _idmenu = "1";
  Color _colorAppBar = Color(0XffA5CE37);

  get getTitulo {
    return _titulo;
  }

  get getDescripcion {
    return _descripcion;
  }

  get getImagen {
    return _imagen;
  }

  get getTipo {
    return _tipo;
  }

  get getOrden {
    return _orden;
  }

  get getUsuario {
    return _usuario;
  }

  get getIdmenu {
    return _idmenu;
  }

  get getColorAppBar {
    return _colorAppBar;
  }

  set setTitulo(String titulo) {
    this._titulo = titulo;
    notifyListeners();
  }

  set setDescripcion(String descripcion) {
    this._descripcion = descripcion;
    notifyListeners();
  }

  set setImagen(String imagen) {
    this._imagen = imagen;
    notifyListeners();
  }

  set setTipo(String tipo) {
    this._tipo = tipo;
    notifyListeners();
  }

  set setOrden(int orden) {
    this._orden = orden;
    notifyListeners();
  }

  set setUsuario(String usuario) {
    this._usuario = usuario;
    notifyListeners();
  }

  set setIdmenu(String idmenu) {
    this._idmenu = idmenu;
    notifyListeners();
  }

  set setColorAppBar(Color colorappbar) {
    this._colorAppBar = colorappbar;
    notifyListeners();
  }
}
