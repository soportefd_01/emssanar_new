import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/helpers/datospaciente.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(child: Lista()),
      bottomNavigationBar: BarraAnimada(),
    );
  }
}

class BarraAnimada extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    final loginProv = Provider.of<LoginProvider>(context);
    final contactoProv = Provider.of<ContactosProvider>(context);
    String numero = "";
    String whatsapp = "";
    acciones(String cadena) async {
      if (await canLaunch(cadena)) {
        await launch(cadena);
      } else {
        throw 'No se completar el proceso ' + cadena;
      }
    }

    return CurvedNavigationBar(
      backgroundColor: kazulOscuro,

      index: 2,
      height: 50,
      /* color: Colors.red, */
      items: <Widget>[
        Image.asset("lib/assets/img/icons/chat.png",
            fit: BoxFit.cover, width: 45, height: 45),
        Image.asset("lib/assets/img/icons/calendario.png",
            fit: BoxFit.cover, width: 40, height: 40),
        Image.asset("lib/assets/img/icons/casa.png",
            fit: BoxFit.cover, width: 40, height: 40),
        Image.asset("lib/assets/img/icons/chat.png",
            fit: BoxFit.cover, width: 40, height: 40),
        Image.asset("lib/assets/img/icons/agente.png",
            fit: BoxFit.cover, width: 40, height: 40),
      ],
      //color: kazulOscuro,
      //buttonBackgroundColor: Colors.black12,
      animationCurve: Curves.easeInOut,
      onTap: (index) {
        //Handle button tap
        print(index);
        switch (index) {
          case 0:
            datosappProv.setTitulo = "Consulta estado de autorización";
            datosappProv.setDescripcion =
                "https://hermes.dyalogo.cloud/crm_php/web_forms.php?web=Mjk4MQ==&paso=69";
            QR.to('/verpaginaRuta');
            break;
          case 1:
            datosappProv.setTitulo = "Solicitud de autorización de servicio";
            datosappProv.setDescripcion =
                "https://hermes.dyalogo.cloud/crm_php/web_forms.php?web=Mjk4MQ==&paso=69";
            QR.to('/verpaginaRuta');
            break;
          case 2:
            whatsapp = "https://wa.me/+573057341404" +
                datosappProv.getDescripcion.toString() +
                "?text=\n¡Hola! \n\n" +
                contactoProv.getNombre.toString() +
                "\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\nCelular: " +
                datosappProv.getDescripcion.toString() +
                "\n\nPor favor interactua con Clara Bot.";
            acciones(whatsapp);
            break;
          case 3:
            datosappProv.setTitulo = "Clarabot";
            datosappProv.setDescripcion = "https://emssanar.org.co/clarabot";
            QR.to('/verpaginaRuta');

            break;
          case 4:
            numero = "tel:018000187050";
            acciones(numero);
            break;
          default:
            print("Opción no valida");
        }
      },
    );
  }
}

class Lista extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(left: 5, top: 50, right: 5),
      children: [
        datospaciente(context),
        d(30),
        pvh(5, 0),
        Center(
          child: Text('¡Nos conectamos contigo!', style: ktituloa),
        ),
        pvh(5, 0),
        _tituloPrincipal(
            context, 'contactenos.png', 'Contáctenos', "1", Color(0Xff2E862A)),
        d(10),
        _tituloPrincipalEstatico(
            context,
            'actualizar_info.png',
            'Actualiza tu información personal',
            "actinfopersonal",
            Color(0xff5CDDFE)),
        d(10),
        _tituloPrincipal(context, 'tramites.png', 'Trámites en línea', "3",
            Color(0Xff4CB6F5)),
        d(10),
        _tituloPrincipalEstatico(context, 'red_de_servicios.png',
            'Mi red de servicios', "redservicios", Color(0xff173EA4)),
        d(10),
        _tituloPrincipalEstatico(context, 'mi_plan.png', 'Mi plan de atención',
            "riastec", Color(0xff623AA2)),
        d(10),
        _tituloPrincipal(context, 'informate_covid.png',
            'Infórmate sobre Covid-19', "6", Color(0XffF97794)),
        d(10),
        _tituloPrincipalEstatico(context, 'eventos.png',
            'Consulta eventos en tu municipio', "eventos", Color(0xffA5CE37)),
        d(10),
        _tituloPrincipal(context, 'danos_tu_opinion.png', 'Danos tu opinión',
            "8", Color(0XffF5BE41)),
        d(10),
        d(10),
      ],
    );
  }
}

Widget _tituloPrincipal(BuildContext context, String nombreImagen,
    String titulo, String idMenu, Color colorAppBar) {
  final datosappProv = Provider.of<DatosAppProvider>(context, listen: false);
  return Card(
    elevation: 2,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset(
        "lib/assets/img/icons/" + nombreImagen,
        height: 60,
        width: 60,
      ),
      title: Text(
        titulo,
        style: ktituloa,
      ),
      trailing: Icon(Icons.arrow_forward),
      onTap: () {
        datosappProv.setTitulo = titulo;
        datosappProv.setColorAppBar = colorAppBar;
        datosappProv.setIdmenu = idMenu;
        QR.to('/submenuEstandarRuta');
      },
    ),
  );
}

Widget _tituloPrincipalEstatico(BuildContext context, String nombreImagen,
    String titulo, String idMenu, Color colorAppBar) {
  final datosappProv = Provider.of<DatosAppProvider>(context, listen: false);
  return Card(
    elevation: 2,
    //color: colorAppBar,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset("lib/assets/img/icons/" + nombreImagen,
        height: 60,
        width: 60,
      ),
      title: Text(
        titulo,
        style: ktituloa,
      ),
      /* subtitle: Text(
        'ESTATICOOOOOOOOO',
        style: ksubtituloa,
      ), */
      trailing: Icon(Icons.arrow_forward),
      onTap: () {
        datosappProv.setTitulo = titulo;
        datosappProv.setColorAppBar = colorAppBar;
        datosappProv.setIdmenu = idMenu;
        //_infPersonalCont();
        if (idMenu == "actinfopersonal") {
          QR.to('/menuactualizarinfoRuta');
        } else if (idMenu == "redservicios") {
          QR.to('/menuredserviciosRuta');
        } else if (idMenu == "riastec") {
          QR.to('/menuriastecnoligiasRuta');
        } else if (idMenu == "eventos") {
          QR.to('/menueventosRuta');
        }
      },
    ),
  );
  /* (
    padding: const EdgeInsets.only(top: 5, bottom: 3),
    child: Text(titulo, style: ktituloa),
  ); */
}

/* Widget _prueba(BuildContext context) {
  final loginProv = Provider.of<LoginProvider>(context);
  return Container(
    //margin: EdgeInsets.symmetric(vertical: 20.0),

    height: 135.0,
    child: ListView(
      scrollDirection: Axis.horizontal,
      /* padding: EdgeInsets.all(5), */
      children: <Widget>[
        //QButton("Menú principal", () => QR.to('/parent')),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: ElevatedButton(
              child: Text('Salir'),
              onPressed: () {
                if (loginProv.getEstaAutenticado) {
                  loginProv.setEstaAutenticado = false;
                  QR.to('/');
                  print('VARIABLE: ' + loginProv.getEstaAutenticado.toString());
                }
              }),
        ),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: ElevatedButton(
              child: Text('Actualizar\nDatos'),
              onPressed: () {
                QR.to('/actpacienteRuta');
              }),
        ),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: ElevatedButton(
              child: Text('Interaccion\nCovid'),
              onPressed: () {
                QR.to('/interaccioncovidRuta');
              }),
        ),
      ],
    ),
  );
} */

//Cirulo inicial con fondo de imagen
/* Widget _buildStack() => Stack(
      alignment: Alignment.center,
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(
              'https://cdn-3.expansion.mx/dims4/default/7ba83a4/2147483647/strip/true/crop/1254x836+0+0/resize/1800x1200!/quality/90/?url=https%3A%2F%2Fcdn-3.expansion.mx%2F45%2F21%2Fa113182c420591587e771a5309e4%2Fistock-928162108.jpg'),
          radius: 150,
        ),
        Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: Colors.black38,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(
            'Bienvenido',
            style: ktitulov,
          ),
        ),
      ],
    ); */

/* // */
