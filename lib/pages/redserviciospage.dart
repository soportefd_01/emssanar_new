/* import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/fondos.dart';
import 'package:emssanar_new/helpers/datospaciente.dart';
import 'package:qlevar_router/qlevar_router.dart';

class RedServiciosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          HeaderWaves(),
          redServicios(context),
          //header(context),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.to("/homeRuta");
        },
      ),
    );
  }
}

ListView redServicios(BuildContext context) => ListView(
      children: [
        pt(10),
        Icon(
          Icons.medical_services,
          size: 100,
          color: kazulOscuro,
        ),
        pt(7),
        Text(
          'Prestador',
          style: ksubtituloa,
          textAlign: TextAlign.center,
        ),
        Text(
          'COOEMSANAR',
          style: ktituloa,
          textAlign: TextAlign.center,
        ),
        pt(70),
        datospaciente(context),
        pt(40),
        Text('MEDICAMENTOS', style: ktituloa, textAlign: TextAlign.center),
        _tile('Medicamento 1', 'Dosificación 1 c/8h',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('Medicamento 2', 'Dosificación 1 c/8h',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('Medicamento 3', 'Dosificación 1 c/8h',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('Medicamento 4', 'Dosificación 1 c/8h',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('Medicamento 5', 'Dosificación 1 c/8h',
            Icons.medical_services_rounded, '/homeRuta'),
        pt(50),
        Text('ODONTOLOGÍA', style: ktituloa, textAlign: TextAlign.center),
        _tile('Cooemssanar', 'Descripción odontología', Icons.theater_comedy,
            '/homeRuta'),
        pt(50),
        Text('PROMOCIÓN Y MANTENIMIENTO',
            style: ktituloa, textAlign: TextAlign.center),
        _tile('Cooemssanar', 'Descripción detalles', Icons.access_alarms,
            '/homeRuta'),
      ],
    );

ListTile _tile(String title, String subtitle, IconData icon, String pagina) =>
    ListTile(
      leading: iconoVerde(Icons.add_business_rounded),
      trailing: iconoVerde(Icons.arrow_forward_ios_outlined),
      title: Text(
        title,
        //textAlign: TextAlign.center,
        style: ktituloa,
      ),
      onTap: () {
        print('HACER ACCION CON CON PROVIDER');
        QR.to(pagina);
      },
      subtitle: Text(
        subtitle,
        style: ksubtituloa,
      ),
      /* leading: Icon(
        icon,
        color: Colors.blue[500],
      ), */
    );
 */