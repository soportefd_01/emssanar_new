import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:qlevar_router/qlevar_router.dart';

class DatosAppPage extends StatelessWidget {
  const DatosAppPage({Key key}) : super(key: key);

  final String getDatosAppAll = r"""
       query getDatosAppAll{
        datosapp{
          titulo
          descripcion
          imagen
          tipo
          orden
          usuario
        }
      }
  """;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Datos App"),
        backgroundColor: Colors.blue.shade900,
      ),
      body: Query(
        options: QueryOptions(
          document: gql(getDatosAppAll),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }

          if (result.isLoading) {
            return Text('Cargando Datos');
          }
          // it can be either Map or List

          List datosAppGraphQl = result.data['datosapp'];
          //pasar la lista a map y luego usarla con el modelo creada

          if (result.data['datosapp'].length == 0) {
            return Text(
              "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            );
          }
          /* fin validaciones GRAPHQL */
          final datosappProv = Provider.of<DatosAppProvider>(context);
          return ListView.builder(
              itemCount: datosAppGraphQl.length,
              itemBuilder: (context, index) {
                final repository = datosAppGraphQl[index];
                return ListTile(
                  contentPadding: EdgeInsets.symmetric(vertical: 20),
                  title: Text(repository['titulo']),
                  leading: Image.network(repository['imagen']),
                  trailing: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.navigate_next,
                      color: Colors.blue.shade900,
                    ),
                  ),
                  onTap: () {
                    datosappProv.setTitulo = repository['titulo'];
                    datosappProv.setImagen = repository['imagen'];
                    datosappProv.setDescripcion = repository['descripcion'];
                    QR.to('/verpaginaRuta');
                    //Navigator.of(context).pushNamed('/verpaginaRutaRuta');
                  },
                  subtitle: Text(repository['descripcion'] +
                      " - " +
                      datosappProv.getTitulo),
                );

                /* return Text(
                    repository['titulo'] + " - " + repository['descripcion']); */
              });
        },
      ),
    );
  }
}
/* // */
