//import 'package:emssanar_new/validations/validationCampo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/toast.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
//import 'package:emssanar_new/helpers/fondos.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:emssanar_new/models/datosusuariomodel.dart';
import 'dart:ui' as ui;

class EstadoAutorizaciones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0Xff4CB6F5),
        title: Text("Estado de Autorizaciones"),
      ),
      body: Stack(
        children: [
          EmssanarWaves(),
          Body(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFF623AA2),
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginProvider>(context);
    print(loginProvider.getTipoIdentificacion);
    print(loginProvider.getIdentificacion);
    return Query(
        options: QueryOptions(
            document: gql(estadoAutorizaciones),
            variables: {"tipoId": "CC", "identificacion": "1085335179"},
            /*variables: {
              "tipoId": loginProvider.getTipoIdentificacion,
              "identificacion": loginProvider.getTipoIdentificacion
            },*/
            fetchPolicy: FetchPolicy.networkOnly,
            pollInterval: Duration(seconds: 20)),
        builder: (result, {refetch, fetchMore}) {
          if (result.hasException) {
            print(result.exception.graphqlErrors);
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Sin autorizaciones para mostrar (E)."),
                TextButton(onPressed: refetch, child: Icon(Icons.refresh))
              ],
            ));
          }

          if (result.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List data = result.data["autorizaciones"];

          if (data == null) {
            return Center(
                child: Column(
              children: [
                Text("Sin autorizaciones para mostrar."),
                TextButton(onPressed: refetch, child: Icon(Icons.refresh))
              ],
            ));
          }

          return Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  var item = data[index];
                  var solicitud = item["solicitud"];
                  var servicio = item["servicio"];
                  var estado = item["estado"];
                  return Card(
                    child: ListTile(
                      trailing: Icon(Icons.double_arrow),
                      title: Text("($solicitud) $servicio"),
                      subtitle: Text(
                        estado,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color:
                                estado.toString().toUpperCase() == "AUTORIZADO"
                                    ? Colors.green
                                    : Colors.grey),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  DetalleAutorizacion(data[index])),
                        );
                      },
                    ),
                  );
                }),
          );
        });
  }
}

class DetalleAutorizacion extends StatelessWidget {
  Map data;

  DetalleAutorizacion(Map data) {
    this.data = data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0Xff4CB6F5),
        title: Text("Detalle de Autorizacion"),
      ),
      body: Stack(
        children: [
          EmssanarWaves(),
          CardDetalleAutorizacion(data),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFF623AA2),
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}

class CardDetalleAutorizacion extends StatelessWidget {
  String solicitud,
      autorizacion,
      estado,
      fecha_autorizacion,
      codigo_servicio,
      servicio,
      ips_autoriza,
      direccion,
      telefono_citas;

  final TextStyle subtitulo = TextStyle(fontWeight: FontWeight.bold);
  final TextStyle texto = TextStyle(fontWeight: FontWeight.w100);
  final TextStyle texto2 =
      TextStyle(fontWeight: FontWeight.bold, color: Colors.green);

  CardDetalleAutorizacion(Map data) {
    solicitud = data["solicitud"];
    autorizacion = data["autorizacion"];
    estado = data["estado"];
    fecha_autorizacion = data["fechaAutorizacion"];
    codigo_servicio = data["codServicioEmssanar"];
    servicio = data["servicio"];
    ips_autoriza = data["ipsAutoriza"];
    direccion = data["direccion"];
    telefono_citas = data["telefonoCitas"];
  }
  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(text: "Solicitud: ", style: subtitulo),
                TextSpan(text: solicitud + "\n\n", style: texto),
                TextSpan(text: "Autorizacion: ", style: subtitulo),
                TextSpan(text: solicitud + "\n\n", style: texto),
                TextSpan(text: "Estado: ", style: subtitulo),
                TextSpan(text: estado + "\n\n", style: texto2),
                TextSpan(text: "Servicio: ", style: subtitulo),
                TextSpan(
                    text: "$codigo_servicio - $servicio \n\n", style: texto),
                TextSpan(text: "Ips que autoriza: ", style: subtitulo),
                TextSpan(text: "$ips_autoriza\n\n", style: texto),
                TextSpan(text: "Dirección: ", style: subtitulo),
                TextSpan(text: "$direccion\n\n", style: texto),
                TextSpan(text: "Telefono de citas: ", style: subtitulo),
                TextSpan(text: "$telefono_citas\n\n", style: texto),
              ],
            ),
          ),
        ));
  }
}

Widget _actualizarForm(BuildContext context) {
  final tamano = MediaQuery.of(context).size;
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);

  return Query(
    options: QueryOptions(
        document: gql(q2getContactosId),
        variables: {"idPaciente": loginProv.getIdentificacion},
        fetchPolicy: FetchPolicy.cacheFirst
        /* pollInterval: Duration(seconds: 10), */
        //poll vuelve a realizar la peticion en 10 segundos
        ),
    builder: (result, {refetch, fetchMore}) {
      if (result.hasException) {
        print(result.exception.toString());
        return Container(child: Text('Ocurrio un problema'));
      }
      if (result.isLoading && result.data['contactosporId'] != null) {
        return Text('Cargando Datos');
      }
      if (result.data.isNotEmpty) {
        print('DATOS CARGADOS COMPELTAMENTE');
        print('==========DATOS LISTOS' +
            result.data['contactosporId'].toString());
      }
      final user = ContactosporId.fromJson(result.data['contactosporId']);
      /* print(" MODELO sin Provider TID: " + user.tipoIdentificacion.toString());
      print(" MODELO sin Provider IDE: " + user.identificacion.toString());
      print(" MODELO sin Provider NOM: " + user.nombre.toString());
      print(" MODELO sin Provider TEL: " + user.telefono.toString());
      print(" MODELO sin Provider CEL: " + user.celular.toString());
      print(" MODELO sin Provider COR: " + user.email.toString());
      print(" MODELO sin Provider DIR: " + user.direccion.toString()); */

      if (result.data['contactosporId'].length == 0) {
        return Text(
          "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
          style: TextStyle(color: Colors.redAccent),
          textAlign: TextAlign.center,
        );
      }

      /* fin validaciones GRAPHQL */
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
                child: Container(
                    height:
                        50.0)), //le damos espacio desde arriba para que no se sobre exponga

            Icon(
              Icons.person_pin_circle,
              color: kazulOscuro,
              size: 100.0,
            ),
            Text(
              user.nombre,
              style: ktituloa,
              textAlign: TextAlign.center,
            ),
            sbh(5),
            Text('Actualización de datos personales', style: ksubtituloa),
            /* Text(
              "\nTI: " +
                  contactoProv.getTipoIdentificacion +
                  "\nI: " +
                  contactoProv.getIdentificacion +
                  "\nT: " +
                  contactoProv.getTelefono +
                  "\nC: " +
                  contactoProv.getCelular +
                  "\nC: " +
                  contactoProv.getEmail +
                  "\nD: " +
                  contactoProv.getDireccion,
              style: TextStyle(
                  color: kazulOscuro,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ), */

            //creamos un rectangular para meter el usuario y contraseña
            Container(
              width: tamano.width *
                  0.85, //me ocupe el 85% del ancho de la pantalla
              padding: EdgeInsets.symmetric(vertical: 50.0),
              margin: EdgeInsets.symmetric(
                  vertical:
                      20.0), //separar el icono y el nombre del frm ingreso

              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5.0),
                        spreadRadius: 3.0)
                  ]),
              child: Column(
                children: <Widget>[
                  Text(
                    'Mis datos de contacto',
                    style: ktituloa,
                    textAlign: TextAlign.center,
                  ),
                  sbh(20.0),
                  _identificacion(context, user.identificacion),
                  sbh(20.0),
                  _telefono(context, user.telefono),
                  sbh(20.0),
                  _celular(context, user.celular),
                  sbh(20.0),
                  _correo(context, user.email),
                  sbh(20.0),
                  _direccion(context, user.direccion),
                  sbh(20.0),
                  CheckUbicacion(),
                  sbh(20.0),
                  _btbActualizarDatos(context),
                  _btbCancelar(context),
                ],
              ),
            ),
            sbh(30)
          ],
        ),
      );
    },
  );
}
//fin formulario de login

//creamos la caja de texto de identificacion
Widget _identificacion(BuildContext context, String identificacionParam) {
  var ideusu = TextEditingController();
  ideusu.text = identificacionParam;

  return Container(
    height: 58,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      controller: ideusu,
      cursorColor: kverde,
      style: ktitulocampo,
      enabled: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.perm_contact_calendar_rounded,
          color: kazulOscuro,
        ),
        labelText: 'Identificación: ',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '87000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: iconoExito(),
      ),
    ),
  );
}
//fin creamos la caja de texto de identificacion

//creamos la caja de texto de telefono
Widget _telefono(BuildContext context, String telefonoParam) {
  final contactoProv = Provider.of<ContactosProvider>(context);
  //final validationService = Provider.of<ValidacionCampo>(context);
  if (contactoProv.getTelefono == " ") {
    contactoProv.setTelefono = telefonoParam;
  }
  return Container(
    height: (contactoProv.getTelefonoVal.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.number,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.phone_callback,
          color: kazulOscuro,
        ),
        labelText: 'Teléfono: ' + telefonoParam,
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '87000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (contactoProv.getTelefonoVal.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: contactoProv.getTelefonoVal.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        contactoProv.setTelefono = text;
        contactoProv.telefonoValidacion(text);
      },
    ),
  );
}
//fin creamos la caja de texto de telefono

//creamos la caja de texto de celular
Widget _celular(BuildContext context, String celularParam) {
  final contactoProv = Provider.of<ContactosProvider>(context);
  if (contactoProv.getCelular == " ") {
    contactoProv.setCelular = celularParam;
  }
  return Container(
    height: (contactoProv.getCelularVal.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.number,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.phone_android_outlined,
          color: kazulOscuro,
        ),
        labelText: 'Celular: ' + celularParam,
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '3100000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (contactoProv.getCelularVal.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: contactoProv.getCelularVal.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        contactoProv.setCelular = text;
        contactoProv.celularValidacion(text);
      },
    ),
  );
}
//fin creamos la caja de texto de celular

//Creamos la caja de texto tipo correo
Widget _correo(BuildContext context, String correoParam) {
  final contactoProv = Provider.of<ContactosProvider>(context);
  if (contactoProv.getEmail == " ") {
    contactoProv.setEmail = correoParam;
  }
  return Container(
    height: (contactoProv.getCorreoVal.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.emailAddress,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.mark_email_read_sharp,
          color: kazulOscuro,
        ),
        labelText: 'Correo:' + correoParam,
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: 'usuario@mail.com',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (contactoProv.getCorreoVal.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: contactoProv.getCorreoVal.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        contactoProv.setEmail = text;
        contactoProv.correoValidacion(text);
      },
    ),
  );
}

//creamos la caja de texto de dirección
Widget _direccion(BuildContext context, String direccionParam) {
  final contactoProv = Provider.of<ContactosProvider>(context);
  if (contactoProv.getDireccion == " ") {
    contactoProv.setDireccion = direccionParam;
  }
  return Container(
    height: (contactoProv.getDireccionVal.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.add_business_sharp,
          color: kazulOscuro,
        ),
        labelText: 'Dirección residencia: \n' + direccionParam,
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: 'Calle x No xx - xy',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (contactoProv.getDireccionVal.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: contactoProv.getDireccionVal.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        contactoProv.setDireccion = text;
        contactoProv.direccionValidacion(text);
      },
    ),
  );
}
//fin Creamos la caja de texto tipo dirección

//Boton inciar sesión
Widget _btbActualizarDatos(BuildContext context) {
  final contactoProv = Provider.of<ContactosProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);

  return Container(
    child: Mutation(
      options: MutationOptions(
          /* documentNode: gql(m1), */
          document: gql(m2ActualizarPaciente),
          variables: {},
          update: (GraphQLDataProxy cache, QueryResult result) {
            return cache;
          },
          onCompleted: (dynamic resultData) {
            if (resultData == null) {
              toast(
                  context,
                  "Error en la llamada. Por favor ponte en contacto con soporte o intenta mas tarde.",
                  Colors.red);
            } else if (resultData["actualizarInfoPaciente"] == null) {
              toast(
                  context,
                  "El usuario no fue encontrado.\nPor favor verifica tu información",
                  Colors.red);
            } else if (resultData["actualizarInfoPaciente"]["paciente"] ==
                null) {
              toast(
                  context,
                  "No se a podido actualizar la información de contacto",
                  Colors.red);
            } else {
              toast(context, "Datos de usuario actualizados correctamente",
                  kverde);

              /* if (contactoProv.getEstaAutenticado) {
                QR.to('/homeRuta');
                //print('VARIABLE: ' + contactoProv.getEstaAutenticado.toString());
              } else {
                contactoProv.setEstaAutenticado = false;
                QR.to('/');
                //print('VARIABLE: ' + contactoProv.getEstaAutenticado.toString());
              } */
            }
          }),
      builder: (RunMutation runMutation, QueryResult result) {
        if (result.isLoading) {
          return Padding(
            padding: EdgeInsets.all(20.0),
            child: Container(
                alignment: Alignment.center,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(
                        strokeWidth: 7.0,
                      ),
                      Text("Por favor espere ...")
                    ],
                  ),
                )),
          );
        }

        if (result.hasException) {
          print(result.exception.toString());
          return Container(
            child: Text(result.exception.toString()),
          );
        }

        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xFF4CB6F5),
                    Color(0xFF623AA2),
                  ],
                )),
            child: ElevatedButton.icon(
              label: Text(
                'Actualizar',
                style: TextStyle(fontFamily: 'raleway'),
              ),
              icon: Icon(Icons.save_rounded),
              style: ElevatedButton.styleFrom(
                  primary: Colors.transparent,
                  shadowColor: Colors.transparent,
                  elevation: 1,
                  minimumSize: Size(double.infinity * 0.2, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12))),
              onPressed: () {
                runMutation(
                  {
                    "identificacion": loginProv.getIdentificacion,
                    "telefono": contactoProv.getTelefono,
                    "celular": contactoProv.getCelular,
                    "direccion": contactoProv.getDireccion,
                    "email": contactoProv.getEmail
                  },
                );
              },
            ),
          ),
        );
      },
    ),
  );
}

//Boton inciar sesión
//Boton Calcelar
Widget _btbCancelar(BuildContext context) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
    child: Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0xFF623AA2),
              Color(0xFF623AA2),
            ],
          )),
      child: ElevatedButton.icon(
        label: Text('Cancelar', style: TextStyle(fontFamily: 'raleway')),
        icon: Icon(Icons.arrow_back_ios),
        style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            shadowColor: Colors.transparent,
            elevation: 1,
            minimumSize: Size(double.infinity * 0.2, 40),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12))),
        onPressed: () {
          QR.to('/homeRuta');
        },
      ),
    ),
  );
}
//Boton Calcelar

//container inicio emssanar
class EmssanarWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: CustomPaint(
        painter: _EmssanarWavesPainter(),
      ),
    );
  }
}

class _EmssanarWavesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final coordenadas = Path();
    final lapizPintar = Paint()
      ..shader = ui.Gradient.linear(
        Offset(0, 0),
        Offset(250, 250),
        <Color>[
          Color(0Xff4CB6F5).withOpacity(0.9),
          Color(0Xff623AA2).withOpacity(0.9),
        ],
        [
          0.1,
          0.9,
        ],
      );

    //Propiedade
    //lapizPintar.color = Color(0Xff615AAB); //color
    lapizPintar.color = Color.fromRGBO(165, 206, 55, 1); //color
    //lapizPintar.style = PaintingStyle.stroke; //stroke dibujar lineas
    lapizPintar.style = PaintingStyle.fill; //Dibujar o Rellenar
    lapizPintar.strokeWidth = 10; //ancho del lapiz

    //PRIMERA FORMA
    //Diubjamos con el coordenadas y el lapiz
    coordenadas.lineTo(0, size.height * 0.55);

    coordenadas.quadraticBezierTo(size.width * 0.30, size.height * 0.5,
        size.width * 0.30, size.height * 0.30);

    coordenadas.quadraticBezierTo(
        size.width * 0.30, size.height * 0.10, size.width * 0.9, 0);

    coordenadas.lineTo(0, 0);

    /* figura inferior */
    coordenadas.moveTo(0, size.height);
    coordenadas.lineTo(0, size.height);
    coordenadas.lineTo(size.width, size.height);
    coordenadas.lineTo(size.width, size.height * 0.95);
    /* coordenadas.quadraticBezierTo(size.width * 0.65, size.height * 0.4,
        size.width * 0.65, size.height * 0.6); */

    coordenadas.quadraticBezierTo(
      size.width * 0.55,
      size.height * 0.9,
      0,
      size.height,
    );

    coordenadas.lineTo(0, size.height);

    canvas.drawPath(coordenadas, lapizPintar);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    throw false;
  }
}

class CheckUbicacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactoProv = Provider.of<ContactosProvider>(context);

    return Padding(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Flexible(
            child: Text(
              "Enviar datos de mi ubicacion actual",
              style: TextStyle(color: kazulOscuro),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          contactoProv.cargandoUbicacion
              ? SizedBox(
                  height: 15,
                  width: 15,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                )
              : Checkbox(
                  value: contactoProv.ubicacion,
                  onChanged: (value) {
                    contactoProv.ubicacion = value;
                  },
                )
        ],
      ),
    );
  }
}

/* // */
