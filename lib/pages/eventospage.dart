import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:emssanar_new/pages/sections/menuseventos.dart';
import 'package:qlevar_router/qlevar_router.dart';

class EventosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    final contactoProv = Provider.of<ContactosProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Eventos"),
        backgroundColor: datosappProv.getColorAppBar,
      ),
      //body: menuEventos(context, "52001"),
      body: menuEventos(context, contactoProv.getMunicipio),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        backgroundColor: datosappProv.getColorAppBar,
        onPressed: () {
          QR.to("/menueventosRuta");
        },
      ),
    );
  }
}
