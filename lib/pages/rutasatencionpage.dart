import 'package:emssanar_new/pages/sections/menurutaspaciente.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class RutasAtencionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactoProv = Provider.of<ContactosProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Mis rutas de atención"),
        backgroundColor: Color(0xff623AA2),
      ),
      body: menuRutasAtencion(context, contactoProv.getIdentificacion),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        backgroundColor: Color(0xff623AA2),
        onPressed: () {
          QR.to("/menuriastecnoligiasRuta");
        },
      ),
    );
  }
}
