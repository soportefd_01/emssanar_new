import 'package:emssanar_new/pages/sections/menuscitas.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:emssanar_new/pages/sections/contactos.dart';

class CitasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Contactos de la Red"),
      ),
      body: Contactos(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: datosappProv.getColorAppBar,
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}
