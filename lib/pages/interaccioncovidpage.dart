//import 'package:emssanar_new/validations/validationCampo.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/toast.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
//import 'package:emssanar_new/providers/pacienteprovider.dart';
import 'package:emssanar_new/providers/interaccioncovidprovider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/models/datosusuariomodel.dart';

class InteraccionCovidPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          EmssanarWaves(),
          _actualizarForm(context),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0Xff4CB6F5),
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}

Widget _actualizarForm(BuildContext context) {
  final tamano = MediaQuery.of(context).size;
  //final contactoProv = Provider.of<ContactosProvider>(context);
  //final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);

  return Query(
    options: QueryOptions(
        document: gql(q2getContactosId),
        variables: {"idPaciente": loginProv.getIdentificacion},
        fetchPolicy: FetchPolicy.cacheFirst),
    builder: (result, {refetch, fetchMore}) {
      if (result.hasException) {
        print(result.exception.toString());
        return Container(child: Text('Ocurrio un problema'));
      }
      if (result.isLoading) {
        return Text('Cargando Datos');
      }
      final user = ContactosporId.fromJson(result.data['contactosporId']);
      /* print(" MODELO sin Provider TID: " + user.tipoIdentificacion.toString());
      print(" MODELO sin Provider IDE: " + user.identificacion.toString());
      print(" MODELO sin Provider NOM: " + user.nombre.toString());
      print(" MODELO sin Provider TEL: " + user.telefono.toString());
      print(" MODELO sin Provider CEL: " + user.celular.toString());
      print(" MODELO sin Provider COR: " + user.email.toString());
      print(" MODELO sin Provider DIR: " + user.direccion.toString()); */
      if (result.data['contactosporId'].length == 0) {
        return Text(
          "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
          style: TextStyle(color: Colors.redAccent),
          textAlign: TextAlign.center,
        );
      }
      /* fin validaciones GRAPHQL */
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
                child: Container(
                    height:
                        50.0)), //le damos espacio desde arriba para que no se sobre exponga

            Icon(
              Icons.reduce_capacity,
              color: kazulOscuro,
              size: 100.0,
            ),
            Text(
              user.nombre,
              style: ktituloa,
              textAlign: TextAlign.center,
            ),
            sbh(5),
            Text('Interacción COVID19', style: ksubtituloa),
            /* Text(loginProv.getIdentificacion),
            Text(validacionCampoProv.getDescripcion.valor),
            Text((validacionCampoProv.getAceptavacunacionChange) ? 'SI' : 'NO'),
            Text((validacionCampoProv.getVacunadoChange) ? 'SI' : 'NO'),
            Text((validacionCampoProv.getProgramadoIpsChange) ? 'SI' : 'NO'),
            Text(validacionCampoProv.getMotivoRechazo.valor),
            Text(validacionCampoProv.getAcudiente.valor),
            Text(validacionCampoProv.getTelefonoAcudiente.valor), */

            //creamos un rectangular para meter el usuario y contraseña
            Container(
              width: tamano.width *
                  0.85, //me ocupe el 85% del ancho de la pantalla
              padding: EdgeInsets.symmetric(vertical: 50.0),
              margin: EdgeInsets.symmetric(
                  vertical:
                      20.0), //separar el icono y el nombre del frm ingreso

              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5.0),
                        spreadRadius: 3.0)
                  ]),
              child: Column(
                children: <Widget>[
                  Text(
                    'Actualización datos para vacunación COVID19',
                    style: ktituloa,
                    textAlign: TextAlign.center,
                  ),
                  sbh(10.0),
                  Text(
                    'Por favor complete la siguiente información.',
                    style: ksubtituloa,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'Los campos marcados con (*) son obligatorios.',
                    style: ksubtituloa,
                    textAlign: TextAlign.center,
                  ),
                  sbh(20.0),
                  _identificacion(context, user.identificacion.toString()),
                  sbh(20.0),
                  _aceptaVacunacion(context),
                  sbh(20.0),
                  _motivorechazo(context),
                  sbh(20.0),
                  _acudiente(context),
                  sbh(20.0),
                  _telefonoAcudiente(context),
                  sbh(20.0),
                  _reconfirmarMunicipio(context),
                  sbh(20.0),
                  _vacunado(context),
                  sbh(20.0),
                  _programadoconips(context),
                  sbh(20.0),
                  _descripcion(context),
                  sbh(20.0),
                  _btbActualizarDatos(context),
                  _btbCancelar(context),
                ],
              ),
            ),
            sbh(30)
          ],
        ),
      );
    },
  );
}

Widget _identificacion(BuildContext context, String identificacionParam) {
  var ideusu = TextEditingController();
  ideusu.text = identificacionParam;
  //final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  //valida que cuando el campo esta vacio
  /* if (validacionCampoProv.getIdentificacion.valor == "") {
    validacionCampoProv.changeIdentificacion(identificacionParam);
  } */
  //print(validacionCampoProv.getIdentificacion.valor);

  return Container(
    height: 58,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      controller: ideusu,
      cursorColor: kverde,
      style: ktitulocampo,
      enabled: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.perm_contact_calendar_rounded,
          color: kazulOscuro,
        ),
        labelText: 'Identificación: ',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '87000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: iconoExito(),
      ),
    ),
  );
}

Widget _aceptaVacunacion(BuildContext context) {
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  String respuesta;
  Color colorrespuesta;
  if (validacionCampoProv.getAceptavacunacionChange) {
    respuesta = "SI";
    colorrespuesta = kverde;
  } else {
    respuesta = "NO";
    colorrespuesta = Colors.red;
  }

  return CheckboxListTile(
    controlAffinity: ListTileControlAffinity.trailing,
    secondary: Icon(
      Icons.help_outline_sharp,
      color: kazulOscuro,
    ),
    title: Text('¿Acepta ser vacunado?: ' + respuesta,
        style: TextStyle(
            color: colorrespuesta, fontSize: 13, fontFamily: 'raleway')),
    value: validacionCampoProv.getAceptavacunacionChange,
    activeColor: kverde,
    onChanged: (bool value) {
      validacionCampoProv.setAceptavacunacionChange = value;
    },
  );
}

Widget _motivorechazo(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);

  /* if (contactoProv.getTelefono == " ") {
    contactoProv.setTelefono = telefonoParam;
  } */
  return Container(
    height: (validacionCampoProv.getMotivoRechazo.esCorrecto) ? 200 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      enabled: !validacionCampoProv.getAceptavacunacionChange,
      maxLines: 8,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.speaker_notes,
          color: (!validacionCampoProv.getAceptavacunacionChange)
              ? kazulOscuro
              : Colors.grey,
        ),
        labelText: 'Motivo de rechazo: ',
        labelStyle: TextStyle(
            color: (!validacionCampoProv.getAceptavacunacionChange)
                ? kazulOscuro
                : Colors.grey),
        hintText: 'Debido a . . .',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (validacionCampoProv.getMotivoRechazo.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: validacionCampoProv.getMotivoRechazo.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        //contactoProv.setTelefono = text;
        validacionCampoProv.changeMotivoRechazo(text);
      },
    ),
  );
}

Widget _acudiente(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  /* if (contactoProv.getTelefono == " ") {
    contactoProv.setTelefono = telefonoParam;
  } */
  return Container(
    height: (validacionCampoProv.getAcudiente.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.people_alt_outlined,
          color: kazulOscuro,
        ),
        labelText: 'Acuediente (*) : ',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: 'Acompañante . . .',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (validacionCampoProv.getAcudiente.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: validacionCampoProv.getAcudiente.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        //contactoProv.setTelefono = text;
        validacionCampoProv.changeAcudiente(text);
      },
    ),
  );
}

Widget _telefonoAcudiente(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);

  /* if (contactoProv.getTelefono == " ") {
    contactoProv.setTelefono = telefonoParam;
  } */
  return Container(
    height: (validacionCampoProv.getTelefonoAcudiente.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.number,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.phone_callback,
          color: kazulOscuro,
        ),
        labelText: 'Teléfono, celular acudiente (*) :  ',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '387000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (validacionCampoProv.getTelefonoAcudiente.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: validacionCampoProv.getTelefonoAcudiente.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        //contactoProv.setTelefono = text;
        validacionCampoProv.changeTelefonoAcudiente(text);
      },
    ),
  );
}

Widget _reconfirmarMunicipio(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  /* if (contactoProv.getDireccion == " ") {
    contactoProv.setDireccion = direccionParam;
  } */
  return Container(
    height: (validacionCampoProv.getReconfirmarMunicipio.esCorrecto) ? 58 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.location_on,
          color: kazulOscuro,
        ),
        labelText: 'Reconfirmar Municipio (*) :',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (validacionCampoProv.getReconfirmarMunicipio.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: validacionCampoProv.getReconfirmarMunicipio.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        //contactoProv.setDireccion = text;
        validacionCampoProv.changeReconfirmarMunicipio(text);
      },
    ),
  );
}

Widget _vacunado(BuildContext context) {
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  String respuesta;
  Color colorrespuesta;
  if (validacionCampoProv.getVacunadoChange) {
    respuesta = "SI";
    colorrespuesta = Colors.green;
  } else {
    respuesta = "NO";
    colorrespuesta = Colors.red;
  }
  return CheckboxListTile(
    controlAffinity: ListTileControlAffinity.trailing,
    secondary: Icon(
      Icons.help_outline_sharp,
      color: kazulOscuro,
    ),
    title: Text('¿Vacunado? : ' + respuesta,
        style: TextStyle(
          color: colorrespuesta,
          fontSize: 13,
          fontFamily: 'raleway',
        )),
    value: validacionCampoProv.getVacunadoChange,
    activeColor: kverde,
    onChanged: (bool value) {
      print(value);
      validacionCampoProv.setVacunadoChange = value;
    },
  );
}

Widget _programadoconips(BuildContext context) {
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  String respuesta;
  Color colorrespuesta;
  if (validacionCampoProv.getProgramadoIpsChange) {
    respuesta = "SI";
    colorrespuesta = Colors.green;
  } else {
    respuesta = "NO";
    colorrespuesta = Colors.red;
  }
  return CheckboxListTile(
    controlAffinity: ListTileControlAffinity.trailing,
    secondary: Icon(
      Icons.calendar_today,
      color: kazulOscuro,
    ),
    title:
        Text('¿Tiene fecha de vacunación por su IPS ? : ', style: ktitulocampo),
    subtitle: Text('' + respuesta,
        style: TextStyle(
            color: colorrespuesta, fontSize: 13, fontFamily: 'raleway')),
    value: validacionCampoProv.getProgramadoIpsChange,
    activeColor: kverde,
    onChanged: (bool value) {
      validacionCampoProv.setProgramadoIpsChange = value;
    },
  );
}

Widget _descripcion(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);
  /* if (contactoProv.getDireccion == " ") {
    contactoProv.setDireccion = direccionParam;
  } */
  return Container(
    height: (validacionCampoProv.getDescripcion.esCorrecto) ? 200 : 80,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      maxLines: 6,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        icon: Icon(
          Icons.description_rounded,
          color: kazulOscuro,
        ),
        labelText: 'Descripción (*) :',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: (validacionCampoProv.getDescripcion.esCorrecto)
            ? iconoExito()
            : iconoError(),
        errorText: validacionCampoProv.getDescripcion.error,
        errorStyle: kerror,
      ),
      onChanged: (String text) {
        //contactoProv.setDireccion = text;
        validacionCampoProv.changeDescripcion(text);
      },
    ),
  );
}

Widget _btbActualizarDatos(BuildContext context) {
  //final contactoProv = Provider.of<ContactosProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  final validacionCampoProv = Provider.of<InteraccionCovidProvider>(context);

  return Container(
    child: Mutation(
      options: MutationOptions(
          /* documentNode: gql(m1), */
          document: gql(m3InteraccionCovid),
          variables: {},
          update: (GraphQLDataProxy cache, QueryResult result) {
            return cache;
          },
          onCompleted: (dynamic resultData) {
            if (resultData == null) {
              toast(
                  context,
                  "Error en la llamada. Por favor ponte en contacto con soporte o intenta mas tarde.",
                  Colors.red);
            } else if (resultData["interaccionCovid"].length == 0) {
              toast(
                  context,
                  "El usuario no fue encontrado.\nPor favor verifica tu información",
                  Colors.red);
            } else if (resultData["interaccionCovid"]["interaccion"] == null) {
              toast(
                  context,
                  "No se a podido actualizar la información de contacto",
                  Colors.red);
            } else {
              toast(context, "Datos de vacunación actualizados correctamente",
                  Colors.lightGreen);
              QR.to('/homeRuta');
            }
          }),
      builder: (RunMutation runMutation, QueryResult result) {
        if (result.isLoading) {
          return Padding(
            padding: EdgeInsets.all(20.0),
            child: Container(
                alignment: Alignment.center,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(
                        strokeWidth: 7.0,
                      ),
                      Text("Por favor espere ...")
                    ],
                  ),
                )),
          );
        }

        if (result.hasException) {
          print(result.exception.toString());
          return Container(
            child: Text(result.exception.toString()),
          );
        }

        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0XffA5CE37),
                    Color(0Xff4CB6F5),
                  ],
                )),
            child: ElevatedButton.icon(
              label: Text('Enviar Información',
                  style: TextStyle(fontFamily: 'raleway')),
              icon: Icon(Icons.send),
              style: ElevatedButton.styleFrom(
                  primary: Colors.transparent,
                  shadowColor: Colors.transparent,
                  elevation: 1,
                  minimumSize: Size(double.infinity * 0.2, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12))),
              onPressed: () {
                if (!validacionCampoProv.isValid) {
                  //print("FALTAN COMPLETAR CAMPOS");
                  //print(validacionCampoProv.isValid);
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: Text(
                              'Alerta',
                              style: TextStyle(color: Colors.red),
                            ),
                            content: Text(
                              '¡Los campos marcados con (*) son obligatorios!',
                              style: TextStyle(
                                  color: kazulOscuro,
                                  fontFamily: 'Raleway',
                                  fontSize: 14),
                            ),
                          ));
                } else {
                  //print("CAMPOS COMPLETOS");
                  //print(validacionCampoProv.isValid);
                  runMutation(
                    {
                      "identificacion": loginProv.getIdentificacion,
                      "descripcion": validacionCampoProv.getDescripcion.valor,
                      "vacunacion":
                          (validacionCampoProv.getAceptavacunacionChange)
                              ? 'SI'
                              : 'NO',
                      "motivoRechazo":
                          validacionCampoProv.getMotivoRechazo.valor,
                      "acudiente": validacionCampoProv.getAcudiente.valor,
                      "telefonoAcudiente":
                          validacionCampoProv.getTelefonoAcudiente.valor,
                      "reconfirmarMunicipio":
                          validacionCampoProv.getReconfirmarMunicipio.valor,
                      "vacunado":
                          (validacionCampoProv.getVacunadoChange) ? 'SI' : 'NO',
                      "programadoIps":
                          (validacionCampoProv.getProgramadoIpsChange)
                              ? 'SI'
                              : 'NO',
                    },
                  );
                  validacionCampoProv.changeMotivoRechazo(' ');
                  validacionCampoProv.changeAcudiente(' ');
                  validacionCampoProv.changeTelefonoAcudiente(' ');
                  validacionCampoProv.changeReconfirmarMunicipio(' ');
                  validacionCampoProv.changeDescripcion(' ');
                  validacionCampoProv.setAceptavacunacionChange = false;
                  validacionCampoProv.setVacunadoChange = false;
                  validacionCampoProv.setProgramadoIpsChange = false;
                }
              },
            ),
          ),
        );
      },
    ),
  );
}

Widget _btbCancelar(BuildContext context) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
    child: Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0Xff4CB6F5),
              Color(0Xff4CB6F5),
            ],
          )),
      child: ElevatedButton.icon(
        label: Text('Cancelar', style: TextStyle(fontFamily: 'raleway')),
        icon: Icon(Icons.arrow_back_ios),
        style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            shadowColor: Colors.transparent,
            elevation: 1,
            minimumSize: Size(double.infinity * 0.2, 40),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12))),
        onPressed: () {
          QR.to('/homeRuta');
        },
      ),
    ),
  );
}

//container inicio emssanar
class EmssanarWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: CustomPaint(
        painter: _EmssanarWavesPainter(),
      ),
    );
  }
}

class _EmssanarWavesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final coordenadas = Path();
    final lapizPintar = Paint()
      ..shader = ui.Gradient.linear(
        Offset(0, 0),
        Offset(250, 250),
        <Color>[
          Color(0XffA5CE37).withOpacity(0.9),
          Color(0Xff4CB6F5).withOpacity(0.9),
        ],
        [
          0.1,
          0.9,
        ],
      );

    //Propiedade
    lapizPintar.color = kverde; //color
    //lapizPintar.color = Color(0Xff615AAB); //color
    //lapizPintar.color = Color.fromRGBO(165, 206, 55, 1); //color
    //lapizPintar.style = PaintingStyle.stroke; //stroke dibujar lineas
    lapizPintar.style = PaintingStyle.fill; //Dibujar o Rellenar
    lapizPintar.strokeWidth = 10; //ancho del lapiz

    //PRIMERA FORMA
    //Diubjamos con el coordenadas y el lapiz
    coordenadas.lineTo(0, size.height * 0.55);

    coordenadas.quadraticBezierTo(size.width * 0.30, size.height * 0.5,
        size.width * 0.30, size.height * 0.30);

    coordenadas.quadraticBezierTo(
        size.width * 0.30, size.height * 0.10, size.width * 0.9, 0);

    coordenadas.lineTo(0, 0);

    /* figura inferior */
    coordenadas.moveTo(0, size.height);
    coordenadas.lineTo(0, size.height);
    coordenadas.lineTo(size.width, size.height);
    coordenadas.lineTo(size.width, size.height * 0.95);
    /* coordenadas.quadraticBezierTo(size.width * 0.65, size.height * 0.4,
        size.width * 0.65, size.height * 0.6); */

    coordenadas.quadraticBezierTo(
      size.width * 0.55,
      size.height * 0.9,
      0,
      size.height,
    );

    coordenadas.lineTo(0, size.height);

    canvas.drawPath(coordenadas, lapizPintar);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    throw false;
  }
}
//
