//import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//import 'package:qlevar_router/qlevar_router.dart';
//import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
//import 'package:emssanar_new/helpers/datospaciente.dart';
//import 'package:url_launcher/url_launcher.dart';

Widget menuRutasAtencion(BuildContext context, String identificacion) {
  return Container(
    height: double.maxFinite,
    //color: Colors.red,
    child: Query(
      options: QueryOptions(
          document: gql(q5getRutasPaciente),
          variables: {"identificacion": identificacion},
          //fetchPolicy: FetchPolicy.cacheFirst),//carga del cache si esta disponible, sino trae los datos de internet
          fetchPolicy: FetchPolicy.networkOnly),
      builder: (QueryResult result,
          {VoidCallback refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }

        if (result.isLoading) {
          return Center(
              child: Text(
            'Cargando Datos',
            style: ktituloa,
          ));
        }
        // it can be either Map or List

        List datosAppGraphQl = result.data['rutasPaciente'];
        //print(datosAppGraphQl);

        //pasar la lista a map y luego usarla con el modelo creada

        if (datosAppGraphQl.length == 0) {
          return Center(
            child: Text(
              "Usted no tiene rutas asignadas",
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            ),
          );
        }
        /* fin validaciones GRAPHQL */

        return ListView.builder(
            itemCount: datosAppGraphQl.length,
            itemBuilder: (context, index) {
              final repository = datosAppGraphQl[index];
              return tileMenu(
                  context,
                  repository['identificacion'],
                  repository['ruta'],
                  repository['procedimiento'],
                  repository['ips'],
                  repository['cumplimiento']);
            });
      },
    ),
  );
}

Card tileMenu(BuildContext context, String identificacion, String ruta,
    String procedimiento, String contacto, String cumplimiento) {
  /* final datosappProv = Provider.of<DatosAppProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  String numero = "";
  String correo = "";
  String whatsapp = "";

  acciones(String cadena) async {
    if (await canLaunch(cadena)) {
      await launch(cadena);
    } else {
      throw 'No se completar el proceso ' + cadena;
    }
  } */

  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset(
        "lib/assets/img/icons/ria.png",
        fit: BoxFit.cover,
        alignment: Alignment.center,
      ),
      /* leading: Image.network(
        procedimiento,
        fit: BoxFit.cover,
      ), */
      //leading: FlutterLogo(),
      //trailing: Icon(Icons.arrow_forward),
      title: Text(
        ruta,
        //textAlign: TextAlign.center,
        style: ktituloa,
      ),

      subtitle: Text(
        "\n" +
            contacto +
            "\n\nPROCEDIMIENTO : " +
            procedimiento +
            "\n\nESTADO : " +
            cumplimiento +
            "\n",
        style: ksubtituloa,
      ),

      onTap: () async => {
        //print('LLAMANDO A URL CON PROVIDER: ' + contacto),
        /*  datosappProv.setDescripcion = contacto,
        datosappProv.setTitulo = identificacion,
        datosappProv.setTipo = cumplimiento,
        //print("TIPO : " + datosappProv.getTipo),
        if (datosappProv.getTipo == 'L')
          {
            QR.to('/verpaginaRuta'),
          }
        else if (datosappProv.getTipo == 'I')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'V')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'N')
          {
            numero = "tel:" + datosappProv.getDescripcion.toString(),
            acciones(numero),
          }
        else if (datosappProv.getTipo == 'S')
          {
            whatsapp = "https://wa.me/+57" +
                datosappProv.getDescripcion.toString() +
                "?text=\nPor favor necesito ayuda sobre: \n\n1. ...\n2. ... \n\nMis datos son: \n\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\n\n Mi número de celular es: " +
                datosappProv.getDescripcion.toString(),
            acciones(whatsapp),
          }
        else if (datosappProv.getTipo == 'C')
          {
            correo = "mailto:" +
                datosappProv.getDescripcion +
                "?subject=Solicitud desde APP&body=\nSeñores: \n\nEMSSANAR EPS. " +
                "\n\nPor favor necesito . . .\n\n1.\n2.\n3.\n\nx-solicitud\n\n\n\n" +
                loginProv.getTipoIdentificacion +
                " : " +
                loginProv.getIdentificacion +
                "\n\nMuchas gracias.",
            acciones(correo),
          }
        else
          {
            print("OPCION NO VALIDA"),
          } */
      },
      /* subtitle: Text(ruta), */
      /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
    ),
  );
}
