import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/menusdinamicos.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class SubMenuEstandar extends StatelessWidget {
  //const MenuContactenos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(datosappProv.getTitulo),
        backgroundColor: datosappProv.getColorAppBar,
      ),
      body: datosAppCont(context, datosappProv.getIdmenu),
      floatingActionButton: FloatingActionButton(
        backgroundColor: datosappProv.getColorAppBar,
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}
