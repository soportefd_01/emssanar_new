import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class MenuActuaizarInfoPersonal extends StatelessWidget {
  //const MenuInfoPersonal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(datosappProv.getTitulo),
        backgroundColor: datosappProv.getColorAppBar,
      ),
      //body: datosAppCont(context, datosappProv.getIdmenu),
      body: _infPersonalCont(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: datosappProv.getColorAppBar,
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.to("/homeRuta");
        },
      ),
    );
  }
}

Widget _infPersonalCont() {
  return Container(
    //margin: EdgeInsets.symmetric(vertical: 20.0),
    //color: Colors.green,
    height: double.infinity,
    child: ListView(
      //scrollDirection: Axis.horizontal,
      /* padding: EdgeInsets.all(5), */
      children: <Widget>[
        _tile('Actualizar mis datos ', 'subti', "actualizar_datos.png",
            '/actpacienteRuta'),
        _tile('¿Te llamamos?, ¡Escribenos!', 'subti', "actualizar_datos.png",
            '/interaccionappRuta'),
      ],
    ),
  );
}

//titulo estatico
Widget _tile(String title, String subtitle, String icon, String pagina) => Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: ListTile(
        leading: Image.asset(
          "lib/assets/img/icons/" + icon,
          fit: BoxFit.cover,
          alignment: Alignment.center,
        ),
        title: Text(
          title,
          style: ktituloa,
        ),
        trailing: Icon(Icons.arrow_forward),
        onTap: () {
          print('HACER ACCION CON CON PROVIDER');
          QR.to(pagina);
        },
        /* subtitle: Text(subtitle), */
        /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
      ),
    );
