import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/providers/contactosRedProvider.dart';
import 'package:url_launcher/url_launcher.dart';

class Contactos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: DropdownMunicipios(),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: DropdownPrestadores(),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: DropdownTipoRed(),
        ),
        Expanded(child: ListaContactos())
      ],
    );
  }
}

class DropdownMunicipios extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactos = Provider.of<RedProvider>(context);

    return Query(
        options: QueryOptions(
          document: gql(municipios),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            print(result.exception);
            return Text("");
          }

          if (result.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List municipios = [
            {"id": "", "nombre": "[ Municipio ]"}
          ];
          municipios.addAll(result.data["municipios"]);

          return DropdownButton(
              isExpanded: true,
              icon: Icon(Icons.arrow_downward, size: 15,),
              value: contactos.municipio,
              iconSize: 24,
              elevation: 16,
              onChanged: (value) {
                contactos.municipio = value;
              },
              items: List.generate(
                  municipios.length,
                      (index) => DropdownMenuItem(
                    value: municipios[index]["id"],
                    child: Text(
                      municipios[index]["nombre"],
                      style: TextStyle(
                          color: index == 0 ? Colors.grey : Colors.black,
                          fontSize: 10),
                    ),
                  )));
        });
  }
}

class DropdownPrestadores extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactos = Provider.of<RedProvider>(context);

    return Query(
        options: QueryOptions(
          document: gql(municipios),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            print(result.exception);
            return Text("");
          }

          if (result.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List prestdores = [
            {"id": "", "nombre": "[ Prestador ]"}
          ];

          return DropdownButton(
              isExpanded: true,
              icon: Icon(Icons.arrow_downward, size: 15,),
              value: contactos.prestador,
              iconSize: 24,
              elevation: 16,
              onChanged: (value) {
                contactos.prestador = value;
              },
              items: List.generate(
                  prestdores.length,
                      (index) => DropdownMenuItem(
                    value: prestdores[index]["id"],
                    child: Text(
                      prestdores[index]["nombre"],
                      style: TextStyle(
                          color: index == 0 ? Colors.grey : Colors.black,
                          fontSize: 10),
                    ),
                  )));
        });
  }
}

class DropdownTipoRed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactos = Provider.of<RedProvider>(context);

    return Query(
        options: QueryOptions(
          document: gql(municipios),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          List tiposRed = [
            {"id": -1, "nombre": "[ Tipo de red ]"}
          ];

          if (result.hasException) {
            return DropdownButton(
                isExpanded: true,
                icon: Icon(Icons.arrow_downward),
                value: contactos.tipoRed,
                iconSize: 24,
                elevation: 16,
                onChanged: (value) {
                  contactos.prestador = value;
                },
                items: List.generate(
                    tiposRed.length,
                        (index) => DropdownMenuItem(
                      value: tiposRed[index]["id"],
                      child: Text(
                        tiposRed[index]["nombre"],
                        style: TextStyle(
                            color: index == 0 ? Colors.grey : Colors.black,
                            fontSize: 10),
                      ),
                    )));
          }

          if (result.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          return DropdownButton(
              isExpanded: true,
              icon: Icon(Icons.arrow_downward, size: 15,),
              value: contactos.tipoRed,
              iconSize: 24,
              elevation: 16,
              onChanged: (value) {
                contactos.prestador = value;
              },
              items: List.generate(
                  tiposRed.length,
                      (index) => DropdownMenuItem(
                    value: tiposRed[index]["id"],
                    child: Text(
                      tiposRed[index]["nombre"],
                      style: TextStyle(
                          color: index == 0 ? Colors.grey : Colors.black,
                          fontSize: 10),
                    ),
                  )));
        });
  }
}

class ListaContactos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final contactoProv = Provider.of<ContactosProvider>(context);

    return Query(
      options: QueryOptions(
          document: gql(q3getContactosIps),
          variables: {"ips": contactoProv.getIps},
          fetchPolicy: FetchPolicy.networkOnly),
      builder: (QueryResult result,
          {VoidCallback refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }

        if (result.isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        List datosAppGraphQl = result.data['contactosIps'];
        print(datosAppGraphQl);
        print(datosAppGraphQl.length);

        if (datosAppGraphQl.length == 0) {
          return Center(
            child: Text(
              "No hay prestadores asignados aún para citas! ",
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            ),
          );
        }

        return Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton.icon(
                    onPressed: refetch,
                    icon: Icon(Icons.search),
                    label: Text("Buscar"),
                  )
                ],
              ),
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: datosAppGraphQl.length,
                    itemBuilder: (context, index) {
                      final repository = datosAppGraphQl[index];
                      return tileMenu(
                          context,
                          repository['ips'],
                          repository['nombreIps'],
                          repository['tipo'],
                          repository['contacto'],
                          repository['observacion']);
                    }))
          ],
        );
      },
    );
  }
}

Card tileMenu(BuildContext context, String ips, String nombreIps, String tipo,
    String contacto, String observacion) {
  final datosappProv = Provider.of<DatosAppProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  String numero = "";
  String correo = "";
  //String whatsapp = "";

  acciones(String cadena) async {
    if (await canLaunch(cadena)) {
      await launch(cadena);
    } else {
      throw 'No se completar el proceso ' + cadena;
    }
  }

  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset(
        "lib/assets/img/icons/citas.png",
        fit: BoxFit.cover,
        alignment: Alignment.center,
      ),
      trailing: Icon(Icons.touch_app),
      title: Text(
        observacion,
        style: ktituloa,
      ),
      subtitle: Text(
        tipo + " : " + contacto + "\nIPS: " + nombreIps,
        style: ksubtituloa,
      ),
      onTap: () async => {
        //print('LLAMANDO A URL CON PROVIDER: ' + contacto),
        datosappProv.setDescripcion = contacto,
        datosappProv.setTitulo = ips,
        datosappProv.setTipo = tipo,
        //print("TIPO : " + datosappProv.getTipo),
        if (datosappProv.getTipo == 'L')
          {
            QR.to('/verpaginaRuta'),
          }
        else /* if (datosappProv.getTipo == 'I')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'V')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else  */
          if (datosappProv.getTipo == 'Telefono Fijo' ||
              datosappProv.getTipo == "Telefono Celular" ||
              datosappProv.getTipo == "Cancelacion de Citas")
            {
              numero = "tel:" + datosappProv.getDescripcion.toString(),
              acciones(numero),
            }
          /* else if (datosappProv.getTipo == 'S')
          {
            whatsapp = "https://wa.me/+57" +
                datosappProv.getDescripcion.toString() +
                "?text=\nPor favor necesito ayuda sobre: \n\n1. ...\n2. ... \n\nMis datos son: \n\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\n\n Mi número de celular es: " +
                datosappProv.getDescripcion.toString(),
            acciones(whatsapp),
          } */
          else if (datosappProv.getTipo == 'Correo Electronico')
            {
              correo = "mailto:" +
                  datosappProv.getDescripcion +
                  "?subject=Solicitud desde APP&body=\nSeñores: \n\nEMSSANAR EPS. " +
                  "\n\nPor favor necesito . . .\n\n1.\n2.\n3.\n\nx-solicitud\n\n\n\n" +
                  loginProv.getTipoIdentificacion +
                  " : " +
                  loginProv.getIdentificacion +
                  "\n\nMuchas gracias.",
              acciones(correo),
            }
          else
            {
              print("OPCION NO VALIDA"),
            }
      },
      /* subtitle: Text(nombreIps), */
      /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
    ),
  );
}
