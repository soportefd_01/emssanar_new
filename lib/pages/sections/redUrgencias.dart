import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_map/flutter_map.dart';

class RedUrgenciasMapa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Red de Urgencias"),
      ),
      body: _RedUrgenciasMapa(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: datosappProv.getColorAppBar,
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}

class _RedUrgenciasMapa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: DropdownTiposRed(),
        ),
        Expanded(child: MapaRed())
      ],
    );
  }
}

class DropdownTiposRed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
          document: gql(tiposRed),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            print(result.exception);
          }

          if (result.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List tiposRed = [
            {"id": "", "nombreTipoRed": "[ Tipos de Red ]"}
          ];
          tiposRed.addAll(result.data["tipoRedApp"]);
          print(result.data);

          return DropdownButton(
              isExpanded: true,
              icon: Icon(Icons.arrow_downward, size: 15,),
              value: "",
              iconSize: 24,
              elevation: 16,
              onChanged: (value) {

              },
              items: List.generate(
                  tiposRed.length,
                      (index) => DropdownMenuItem(
                    value: tiposRed[index]["id"],
                    child: Text(
                      tiposRed[index]["nombreTipoRed"],
                      style: TextStyle(
                          color: index == 0 ? Colors.grey : Colors.black,
                          fontSize: 10),
                    ),
                  )));
        });
  }
}

class MapaRed extends StatelessWidget {

  Future<String> a () async{
    return "a";
  }
  @override
  Widget build(BuildContext context) {
    final contactoProv = Provider.of<ContactosProvider>(context);

    return FutureBuilder(
      future: a(),
      builder: (context, snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          return FlutterMap(
            options: MapOptions(
              zoom: 13.0,
            ),
            layers: [
              TileLayerOptions(
                  urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c']
              ),
              MarkerLayerOptions(
                markers: [
                  Marker(
                    width: 80.0,
                    height: 80.0,
                    builder: (ctx) =>
                        Container(
                          child: FlutterLogo(),
                        ),
                  ),
                ],
              ),
            ],
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

Card tileMenu(BuildContext context, String ips, String nombreIps, String tipo,
    String contacto, String observacion) {
  final datosappProv = Provider.of<DatosAppProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  String numero = "";
  String correo = "";
  //String whatsapp = "";

  acciones(String cadena) async {
    if (await canLaunch(cadena)) {
      await launch(cadena);
    } else {
      throw 'No se completar el proceso ' + cadena;
    }
  }

  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset(
        "lib/assets/img/icons/citas.png",
        fit: BoxFit.cover,
        alignment: Alignment.center,
      ),
      trailing: Icon(Icons.touch_app),
      title: Text(
        observacion,
        style: ktituloa,
      ),
      subtitle: Text(
        tipo + " : " + contacto + "\nIPS: " + nombreIps,
        style: ksubtituloa,
      ),
      onTap: () async => {
        //print('LLAMANDO A URL CON PROVIDER: ' + contacto),
        datosappProv.setDescripcion = contacto,
        datosappProv.setTitulo = ips,
        datosappProv.setTipo = tipo,
        //print("TIPO : " + datosappProv.getTipo),
        if (datosappProv.getTipo == 'L')
          {
            QR.to('/verpaginaRuta'),
          }
        else /* if (datosappProv.getTipo == 'I')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'V')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else  */
          if (datosappProv.getTipo == 'Telefono Fijo' ||
              datosappProv.getTipo == "Telefono Celular" ||
              datosappProv.getTipo == "Cancelacion de Citas")
            {
              numero = "tel:" + datosappProv.getDescripcion.toString(),
              acciones(numero),
            }
          /* else if (datosappProv.getTipo == 'S')
          {
            whatsapp = "https://wa.me/+57" +
                datosappProv.getDescripcion.toString() +
                "?text=\nPor favor necesito ayuda sobre: \n\n1. ...\n2. ... \n\nMis datos son: \n\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\n\n Mi número de celular es: " +
                datosappProv.getDescripcion.toString(),
            acciones(whatsapp),
          } */
          else if (datosappProv.getTipo == 'Correo Electronico')
            {
              correo = "mailto:" +
                  datosappProv.getDescripcion +
                  "?subject=Solicitud desde APP&body=\nSeñores: \n\nEMSSANAR EPS. " +
                  "\n\nPor favor necesito . . .\n\n1.\n2.\n3.\n\nx-solicitud\n\n\n\n" +
                  loginProv.getTipoIdentificacion +
                  " : " +
                  loginProv.getIdentificacion +
                  "\n\nMuchas gracias.",
              acciones(correo),
            }
          else
            {
              print("OPCION NO VALIDA"),
            }
      },
      /* subtitle: Text(nombreIps), */
      /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
    ),
  );
}
