import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class RiasTecnologias extends StatelessWidget {
  //  const RiasTecnologias({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(datosappProv.getTitulo),
        backgroundColor: datosappProv.getColorAppBar,
      ),
      //body: datosAppCont(context, datosappProv.getIdmenu),
      body: _planAtenciCont(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        backgroundColor: datosappProv.getColorAppBar,
        onPressed: () {
          QR.to("/homeRuta");
        },
      ),
    );
  }
}

Widget _planAtenciCont() {
  return Container(
    /* decoration: BoxDecoration(
        gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        Color(0xFF4CB6F5),
        Color(0xFF623AA2),
      ],
    )), */
    height: double.infinity,
    child: ListView(
      //scrollDirection: Axis.horizontal,
      /* padding: EdgeInsets.all(5), */
      children: <Widget>[
        /* _contenedorIcono('Tecnologías, RIA, promoción y mantenimiento',
            "ria.png", '/tecnologiasRuta'), */
        _contenedorIcono(
            'Rutas de atención', "ruta_promocion.png", '/rutasatencionRuta'),
      ],
    ),
  );
}

//contenedor estatico
Container _contenedorIcono(String titulo, String iconoleading, String pagina) =>
    Container(
      width: 120.0,
      margin: EdgeInsets.all(7.0),
      /* margin: EdgeInsets.symmetric(vertical: 7), */
      /* padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0), */
      padding: EdgeInsets.only(top: 2),
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _tile(titulo, 'subtitulo', iconoleading, pagina),
        ],
      ),
    );
//titulo estatico
Widget _tile(
        String title, String subtitle, String iconoleading, String pagina) =>
    Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: ListTile(
        leading: Image.asset(
          "lib/assets/img/icons/" + iconoleading,
          fit: BoxFit.cover,
          alignment: Alignment.center,
        ),
        title: Text(
          title,
          style: ktituloa,
        ),
        trailing: Icon(Icons.arrow_forward),
        onTap: () {
          print('HACER ACCION CON CON PROVIDER');
          QR.to(pagina);
        },
        /* subtitle: Text(subtitle), */
        /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
      ),
    );
