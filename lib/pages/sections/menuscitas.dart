import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
//import 'package:emssanar_new/helpers/datospaciente.dart';
import 'package:url_launcher/url_launcher.dart';

Widget menuCitas(BuildContext context, String ips) {
  return Container(
    height: double.maxFinite,
    //color: Colors.red,
    child: Query(
      options: QueryOptions(
          document: gql(q3getContactosIps),
          //variables: {"ips": ips},
          variables: {"ips": ips},
          //fetchPolicy: FetchPolicy.cacheFirst),//carga del cache si esta disponible, sino trae los datos de internet
          fetchPolicy: FetchPolicy.networkOnly),
      builder: (QueryResult result,
          {VoidCallback refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }

        if (result.isLoading) {
          return Center(
              child: Text(
            'Cargando Datos',
            style: ktituloa,
          ));
        }
        // it can be either Map or List

        List datosAppGraphQl = result.data['contactosIps'];
        print(datosAppGraphQl);
        print(datosAppGraphQl.length);

        //pasar la lista a map y luego usarla con el modelo creada

        if (datosAppGraphQl.length == 0) {
          return Center(
            child: Text(
              "No hay prestadores asignados aún para citas! ",
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            ),
          );
        }
        /* fin validaciones GRAPHQL */

        return ListView.builder(
            itemCount: datosAppGraphQl.length,
            itemBuilder: (context, index) {
              final repository = datosAppGraphQl[index];
              return tileMenu(
                  context,
                  repository['ips'],
                  repository['nombreIps'],
                  repository['tipo'],
                  repository['contacto'],
                  repository['observacion']);
            });
      },
    ),
  );
}

Card tileMenu(BuildContext context, String ips, String nombreIps, String tipo,
    String contacto, String observacion) {
  final datosappProv = Provider.of<DatosAppProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  String numero = "";
  String correo = "";
  //String whatsapp = "";

  acciones(String cadena) async {
    if (await canLaunch(cadena)) {
      await launch(cadena);
    } else {
      throw 'No se completar el proceso ' + cadena;
    }
  }

  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.asset(
        "lib/assets/img/icons/citas.png",
        fit: BoxFit.cover,
        alignment: Alignment.center,
      ),
      trailing: Icon(Icons.touch_app),
      title: Text(
        observacion,
        style: ktituloa,
      ),
      subtitle: Text(
        tipo + " : " + contacto + "\nIPS: " + nombreIps,
        style: ksubtituloa,
      ),
      onTap: () async => {
        //print('LLAMANDO A URL CON PROVIDER: ' + contacto),
        datosappProv.setDescripcion = contacto,
        datosappProv.setTitulo = ips,
        datosappProv.setTipo = tipo,
        //print("TIPO : " + datosappProv.getTipo),
        if (datosappProv.getTipo == 'L')
          {
            QR.to('/verpaginaRuta'),
          }
        else /* if (datosappProv.getTipo == 'I')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'V')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else  */
        if (datosappProv.getTipo == 'Telefono Fijo' ||
            datosappProv.getTipo == "Telefono Celular" ||
            datosappProv.getTipo == "Cancelacion de Citas")
          {
            numero = "tel:" + datosappProv.getDescripcion.toString(),
            acciones(numero),
          }
        /* else if (datosappProv.getTipo == 'S')
          {
            whatsapp = "https://wa.me/+57" +
                datosappProv.getDescripcion.toString() +
                "?text=\nPor favor necesito ayuda sobre: \n\n1. ...\n2. ... \n\nMis datos son: \n\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\n\n Mi número de celular es: " +
                datosappProv.getDescripcion.toString(),
            acciones(whatsapp),
          } */
        else if (datosappProv.getTipo == 'Correo Electronico')
          {
            correo = "mailto:" +
                datosappProv.getDescripcion +
                "?subject=Solicitud desde APP&body=\nSeñores: \n\nEMSSANAR EPS. " +
                "\n\nPor favor necesito . . .\n\n1.\n2.\n3.\n\nx-solicitud\n\n\n\n" +
                loginProv.getTipoIdentificacion +
                " : " +
                loginProv.getIdentificacion +
                "\n\nMuchas gracias.",
            acciones(correo),
          }
        else
          {
            print("OPCION NO VALIDA"),
          }
      },
      /* subtitle: Text(nombreIps), */
      /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
    ),
  );
}
