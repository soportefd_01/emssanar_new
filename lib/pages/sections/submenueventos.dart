import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class MenuEventos extends StatelessWidget {
  //const MenuEventos({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return Scaffold(
      appBar: AppBar(
          title: Text(datosappProv.getTitulo),
          backgroundColor: datosappProv.getColorAppBar),
      //body: datosAppCont(context, datosappProv.getIdmenu),
      body: _eventosMunCont(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: datosappProv.getColorAppBar,
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.to("/homeRuta");
        },
      ),
    );
  }
}

Widget _eventosMunCont() {
  return Container(
    height: double.infinity,
    child: ListView(
      //scrollDirection: Axis.horizontal,
      /* padding: EdgeInsets.all(5), */
      children: <Widget>[
        _contenedorIcono('Eventos', Icons.event, '/eventosRuta'),
      ],
    ),
  );
}

//contenedor estatico
Container _contenedorIcono(String titulo, IconData icono, String pagina) =>
    Container(
      width: 120.0,
      margin: EdgeInsets.all(7.0),
      /* margin: EdgeInsets.symmetric(vertical: 7), */
      /* padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0), */
      padding: EdgeInsets.only(top: 2),
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _tile(titulo, 'subtitulo', Icons.ac_unit, pagina),
        ],
      ),
    );
//titulo estatico
Widget _tile(String title, String subtitle, IconData icon, String pagina) =>
    Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: ListTile(
        leading: Image.asset(
          "lib/assets/img/icons/eventos.png",
          fit: BoxFit.cover,
          alignment: Alignment.center,
        ),
        title: Text(
          title,
          style: ktituloa,
        ),
        trailing: Icon(Icons.arrow_forward),
        onTap: () {
          print('HACER ACCION CON CON PROVIDER');
          QR.to(pagina);
        },
        /* subtitle: Text(subtitle), */
        /* leading: Icon(
          icon,
          color: Colors.blue[500],
        ), */
      ),
    );
