import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/helpers/toast.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:emssanar_new/helpers/tipoIdentificacion.dart';
//import 'package:ems/pages/validarFechaLogin.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _loginForm(context),
        ],
      ),
    );
  }
}

//fondo del login
Widget _crearFondo(BuildContext context) {
  //utilizamos media query para calclar el tamaño de la pantalla
  final tamano = MediaQuery.of(context).size;

  final fondoVerde = Container(
    height: tamano.height * 0.60, //ocupa el 40% de la pantalla en alto height
    width: double.infinity, //ocupa todo el ancho width
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          Color(0Xff2E862A).withOpacity(0.9),
          Color(0XffA5CE37).withOpacity(0.9),
          /* Color.fromRGBO(165, 206, 55, 1),
          Color.fromRGBO(165, 206, 100, 1),
          Color.fromRGBO(4, 92, 150, 0.9), */
          //Color.fromRGBO(255, 255, 255, 5.0),
        ],
      ),
    ),
  );

  //armo los circulos para meterlos dentro del stack
  final circulo = Container(
    width: 100,
    height: 100,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.1)),
  );

  final iconoNombre = Container(
    //bajar la columna con padding
    padding: EdgeInsets.only(top: 10.0),
    child: ListView(
      children: <Widget>[
        //Image.asset('img/ems.png'),
        Container(
          child: Image.asset('lib/assets/img/emssanar_logo.png'),
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 90),
        ),
        /* Icon(
          Icons.accessible,
          /* Icons.person, */
          /* Icons.chrome_reader_mode, */
          color: Colors.white,
          size: 100.0,
        ), */
        /* SizedBox(
            height: 5.0, width: double.infinity), //centrar el icono y el nombre
        Text(
          'Login Emssanar',
          style: TextStyle(color: Colors.white, fontSize: 25.0),
          textAlign: TextAlign.center,
        ) */
      ],
    ),
  );

  return Stack(
    children: <Widget>[
      fondoVerde,
      Positioned(top: 40, left: 50, child: circulo),
      Positioned(top: -40, right: -30, child: circulo),
      Positioned(top: 190, right: 10, child: circulo),
      Positioned(top: 200, left: -35, child: circulo),
      iconoNombre
    ],
  );
}
//fin fondo del login

//formulario de login
Widget _loginForm(BuildContext context) {
  final tamano = MediaQuery.of(context).size;
  //final loginProv = Provider.of<LoginProvider>(context);
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SafeArea(
            child: Container(
          height: 170.0,
        )), //le damos espacio desde arriba para que no se sobre exponga
        //creamos un rectangular para meter el usuario y contraseña
        Container(
          width: tamano.width * 0.85, //me ocupe el 85% del ancho de la pantalla
          padding: EdgeInsets.symmetric(vertical: 50.0),
          margin: EdgeInsets.symmetric(
              vertical: 20.0), //separar el icono y el nombre del frm ingreso

          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              /* Text('Tipo doc: ' + loginProv.getTipoIdentificacion,
                  style: TextStyle(color: Colors.blue.shade900)),
              Text('Identificación: ' + loginProv.getIdentificacion,
                  style: TextStyle(color: Colors.blue.shade900)),
              Text('Correo: ' + loginProv.getUsuario,
                  style: TextStyle(color: Colors.blue.shade900)),
              Text('Contraseña: ' + loginProv.getContrasena,
                  style: TextStyle(color: Colors.blue.shade900)),
              Text(
                'Datos De Acceso',
                style: TextStyle(fontSize: 20.0, color: Colors.blue.shade900),
              ), */
              //SizedBox(height: 20.0),
              Text(
                'Inicio de sesión',
                style: ktituloa,
                textAlign: TextAlign.center,
              ),

              SizedBox(height: 20.0),

              //tipoidentificacion(context),
              //SizedBox(height: 20.0),
              _identificacion(context),
              SizedBox(height: 20.0),
              _contrasena(context),
              SizedBox(height: 20.0),
              _btbIniciarSesion(context),
            ],
          ),
        ),
        //Text('¿Olvido su contraseña?', style: ksubtituloa),
        SizedBox(height: 30.0)
      ],
    ),
  );
}
//fin formulario de login

//creamos la caja de texto de correo
Widget _identificacion(BuildContext context) {
  final loginProv = Provider.of<LoginProvider>(context);
  loginProv.setTipoIdentificacion = 'CC';
  loginProv.setIdentificacion = '98400407';
  loginProv.setContrasena = '01/01/1978';
  return Container(
    height: 50,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        labelText: 'Identificación: ',
        labelStyle: TextStyle(color: kazulOscuro),
        hintText: '87000000',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: Icon(Icons.keyboard, color: Colors.blue.shade900),
        /* icon: Icon(
          Icons.chrome_reader_mode,
          color: Colors.blue.shade900,
        ), */

        //errorText: 'usuario no valido',
      ),
      onChanged: (text) {
        //print("First text field: $text");
        loginProv.setIdentificacion = text;
      },
    ),
  );
}
//fin creamos la caja de texto de correo

//creamos la caja de texto de contraseña
Widget _contrasena(context) {
  final loginProv = Provider.of<LoginProvider>(context);
  return Container(
    height: 50,
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.text,
      style: ktitulocampo,
      //obscureText: true, //ocultar caracteres mienstras escribe
      decoration: InputDecoration(
        focusedBorder: kbordeseleccionado,
        enabledBorder: kbordenormal,
        border: kborderedondeado,
        labelText: 'Fecha Nacimiento dd/mm/aaaa',
        labelStyle: TextStyle(color: Colors.blue.shade900),
        hintText: '*********',
        hintStyle: TextStyle(color: Colors.black26),
        suffix: Icon(Icons.keyboard, color: Colors.blue.shade900),
        /* icon: Icon(
          Icons.lock_outline,
          color: Colors.blue.shade900,
        ), */
      ),
      onChanged: (text) {
        //print("First text field: $text");
        loginProv.setContrasena = text;
      },
    ),
  );
}
//creamos la caja de texto de contraseña

//Boton inciar sesión
Widget _btbIniciarSesion(BuildContext context) {
  final loginProv = Provider.of<LoginProvider>(context);
  return Container(
    child: Mutation(
      options: MutationOptions(
          /* documentNode: gql(m1), */
          document: gql(m1BuscarPaciente),
          update: (GraphQLDataProxy cache, QueryResult result) {
            return cache;
          },
          onCompleted: (dynamic resultData) {
            if (resultData == null) {
              toast(
                  context,
                  "Error en la llamada. Por favor ponte en contacto con soporte o intenta mas tarde.",
                  Colors.red);
            } else if (resultData["buscarPaciente"] == null) {
              toast(
                  context,
                  "El usuario no fue encontrado.\nPor favor verifica tu información",
                  Colors.red);
              resultData = null;
              loginProv.setEstaAutenticado = false;
            } else {
              toast(context, "Usuario autenticado correctamente",
                  Colors.lightGreen);
              loginProv.setId = resultData["buscarPaciente"]["paciente"]["id"];
              loginProv.setEstaAutenticado = true;
              if (loginProv.getEstaAutenticado) {
                QR.to('/homeRuta');
                //print('VARIABLE: ' + loginProv.getEstaAutenticado.toString());
              } else {
                loginProv.setEstaAutenticado = false;
                QR.to('/');
                //print('VARIABLE: ' + loginProv.getEstaAutenticado.toString());
              }
            }
          }),
      builder: (RunMutation runMutation, QueryResult result) {
        if (result.isLoading) {
          return Padding(
            padding: EdgeInsets.all(20.0),
            child: Container(
                alignment: Alignment.center,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(
                        strokeWidth: 7.0,
                      ),
                      Text(
                        "Por favor espere ...",
                        style: ksubtitulov,
                      )
                    ],
                  ),
                )),
          );
        }

        if (result.hasException) {
          print(result.exception.toString());
          return Container(
            child: Text(result.exception.toString()),
          );
        }

        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xFFA5CE37),
                    Color(0xFF2E862A),
                  ],
                )),
            child: ElevatedButton.icon(
              label: Text('Ingresar', style: TextStyle(fontFamily: 'raleway')),
              icon: Icon(Icons.login),
              style: ElevatedButton.styleFrom(
                  primary: Colors.transparent,
                  shadowColor: Colors.transparent,
                  elevation: 1,
                  minimumSize: Size(double.infinity * 0.2, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12))),
              onPressed: () {
                print("..........................."+ loginProv.getIdentificacion);
                runMutation(
                  {
                    "identificacion": loginProv.getIdentificacion,
                    "fechaNacimiento": loginProv.getContrasena
                    //"fechaNacimiento": "01/01/1978"
                  },
                );
              },
            ),
          ),
        );
      },
    ),
  );
}
//Boton inciar sesión
