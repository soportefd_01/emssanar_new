//import 'package:emssanar_new/helpers/ayudas.dart';

import 'package:emssanar_new/pages/sections/menuprestadorespaciente.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';

class PrestadoresPacientePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    final contactoProv = Provider.of<ContactosProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Prestadores",
        ),
      ),
      body: menuPrestadores(context, contactoProv.getIdentificacion),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        backgroundColor: datosappProv.getColorAppBar,
        onPressed: () {
          QR.back();
        },
      ),
    );
  }
}
