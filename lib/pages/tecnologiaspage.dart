import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/fondos.dart';
import 'package:emssanar_new/helpers/datospaciente.dart';
import 'package:qlevar_router/qlevar_router.dart';

class TecnologiasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          HeaderWaves(),
          medicamentos(context),
          //header(context),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () {
          QR.to("/menuriastecnoligiasRuta");
        },
      ),
    );
  }
}

ListView medicamentos(BuildContext context) => ListView(
      children: [
        pt(10),
        Icon(
          Icons.transfer_within_a_station_sharp,
          size: 100,
          color: kazulOscuro,
        ),
        pt(7),
        Text(
          'Prestador',
          style: ksubtituloa,
          textAlign: TextAlign.center,
        ),
        Text(
          'COOEMSANAR',
          style: ktituloa,
          textAlign: TextAlign.center,
        ),
        pt(70),
        datospaciente(context),
        pt(40),
        Text('Ruta del paciente:',
            style: ktituloa, textAlign: TextAlign.center),
        pt(5),
        Text('ADULTES', style: ktituloa, textAlign: TextAlign.center),
        pt(20),
        _tile('MEDICINA GENERAL PRIMERA VEZ', 'Prestador: Cooemssanar',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('ENFERMERIA PRIMERA VEZ', 'Prestador: Cooemssanar',
            Icons.medical_services_rounded, '/homeRuta'),
        _tile('OPTOMETRIA PRIMERA VEZ', 'Prestador: Fundonar',
            Icons.medical_services_rounded, '/homeRuta'),
        pt(50),
      ],
    );

ListTile _tile(String title, String subtitle, IconData icon, String pagina) =>
    ListTile(
      leading: iconoVerde(Icons.account_tree_sharp),
      trailing: iconoVerde(Icons.how_to_reg_outlined),
      //leading: iconoVerde(Icons.reduce_capacity_outlined),
      //trailing: Checkbox(value: true),
      title: Text(
        title,
        //textAlign: TextAlign.center,
        style: ktituloa,
      ),
      onTap: () {
        print('HACER ACCION CON CON PROVIDER');
        QR.to(pagina);
      },
      subtitle: Text(
        subtitle + " \nFecha: 2021/01/10",
        style: ksubtituloa,
      ),
      /* leading: Icon(
        icon,
        color: Colors.blue[500],
      ), */
    );
