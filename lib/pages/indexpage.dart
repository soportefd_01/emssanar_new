//import 'package:emssanar_new/pages/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:emssanar_new/pages/loginpage.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
import 'dart:ui' as ui;

class IndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          FondoEmssanar(),
          LoginPage(),
          //HomePage(),
        ],
      ),
    );
  }
}

//Fondo emssanar
class FondoEmssanar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        EmssanarWaves(),
        ImagenEmssanar(),
        FlechaAbajo(),
      ],
    );
  }
}

//Imagen
class ImagenEmssanar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Image.asset('lib/assets/img/emssanar_logo.png'),
      alignment: Alignment.center,
    );
  }
}

//Flecha abajo
class FlechaAbajo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            sbh(10),
            Text(
              'Bienvenido',
              textAlign: TextAlign.start,
              style: ktituloa,
            ),
            Expanded(child: Container()),
            Text(
              "Deslice hacia arriba",
              style: ksubtituloa,
            ),
            Icon(
              Icons.arrow_upward,
              //color: Color.fromRGBO(165, 206, 55, 0.99),
              color: kverde,
              size: 45.0,
            ),
          ],
        ),
      ),
    );
  }
}

//container inicio emssanar
class EmssanarWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: CustomPaint(
        painter: _EmssanarWavesPainter(),
      ),
    );
  }
}

class _EmssanarWavesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final coordenadas = Path();
    final lapizPintar = Paint()
      ..shader = ui.Gradient.linear(
        Offset(0, 0),
        Offset(250, 250),
        <Color>[
          Color(0Xff2E862A).withOpacity(0.9),
          Color(0XffA5CE37).withOpacity(0.9),
        ],
        [
          0.1,
          0.9,
        ],
      );

    //Propiedade
    //lapizPintar.color = Color(0Xff615AAB); //color
    //lapizPintar.color = Color.fromRGBO(165, 206, 55, 0.9); //color
    //lapizPintar.style = PaintingStyle.stroke; //stroke dibujar lineas
    lapizPintar.style = PaintingStyle.fill; //Dibujar o Rellenar
    lapizPintar.strokeWidth = 10; //ancho del lapiz

    //PRIMERA FORMA
    //Diubjamos con el coordenadas y el lapiz
    coordenadas.lineTo(0, size.height * 0.85);

    coordenadas.quadraticBezierTo(size.width * 0.5, size.height * 0.7,
        size.width * 0.50, size.height * 0.50);

    coordenadas.quadraticBezierTo(
        size.width * 0.5, size.height * 0.10, size.width * 0.8, 0);

    coordenadas.lineTo(0, 0);

    /* coordenadas.moveTo(0, size.height);
    coordenadas.lineTo(0, size.height);
    coordenadas.lineTo(size.width, size.height);
    coordenadas.lineTo(size.width, size.height * 0.25);
    /* coordenadas.quadraticBezierTo(size.width * 0.65, size.height * 0.4,
        size.width * 0.65, size.height * 0.6); */

    coordenadas.quadraticBezierTo(
      size.width * 0.55,
      size.height * 0.9,
      0,
      size.height,
    );

    coordenadas.lineTo(0, size.height); */

    canvas.drawPath(coordenadas, lapizPintar);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    throw true;
  }
}
/* // */
