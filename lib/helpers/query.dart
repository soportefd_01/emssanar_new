const m1BuscarPaciente = """
mutation(

  \$identificacion:String,
  \$fechaNacimiento:String
  )
{
buscarPaciente(

identificacion:\$identificacion,
fechaNacimiento:\$fechaNacimiento
)
{
    paciente{
      id
    }
  }
}
""";

const m2ActualizarPaciente = """
mutation (
  \$identificacion:String!, 
  \$telefono:String!,
  \$celular:String!,
  \$email:String!,
  \$direccion:String!)
{
  actualizarInfoPaciente(
    identificacion:\$identificacion, 
    telefono:\$telefono,
    celular:\$celular,
    email:\$email,
    direccion:\$direccion)
  {
    paciente
    {
      id
      tipoIdentificacion
      identificacion
      nombre
      telefono
      celular
      email
      direccion
      
    }
  }
}

""";

const m3InteraccionCovid = """
mutation(
  \$identificacion:String!,
  \$descripcion:String!,
  \$vacunacion:String!,
  \$motivoRechazo:String,
  \$acudiente:String!,
  \$telefonoAcudiente:String!,
  \$reconfirmarMunicipio:String!,
  \$vacunado:String!,
  \$programadoIps:String!,
)
{
  interaccionCovid(
    identificacion:\$identificacion,
    descripcion:\$descripcion,
    vacunacion:\$vacunacion,
    motivoRechazo:\$motivoRechazo,
		acudiente:\$acudiente,
    telefonoAcudiente:\$telefonoAcudiente,
    reconfirmarMunicipio:\$reconfirmarMunicipio,
    vacunado:\$vacunado,
    programadoIps:\$programadoIps,
    )
  {
    interaccion{
      id      
      identificacion
      descripcion
      tipoInteraccion
      clasificacion
      prioridad
      medioRecepcion
      personaRegistro
      
    }
  }
}""";

const String m5 = """
mutation(\$idPaciente:Int, \$fechaNacimiento:String){
  validarPaciente(idPaciente:\$idPaciente, fechaNacimiento:\$fechaNacimiento){
    validacion
  }
}
""";

const String m6CrearInteraccionAPP = """
mutation crearInteraccionAPP (
      \$descripcion:String!,
      \$identificacion:String!,
      \$tipoInteraccion:Int!,
      )
{  crearInteraccionAPP(
  descripcion:\$descripcion,
  identificacion:\$identificacion,
  tipoInteraccion:\$tipoInteraccion,
  )
        {    
                interaccion 
                {      
                        id
                        identificacion      
                        descripcion      
                        tipoInteraccion      
                        clasificacion      
                        prioridad      
                        medioRecepcion      
                        personaRegistro    
                }
        }
}
""";

const String q1getDatosAppAll = r"""
       query getDatosAppAll($menu:String!){
        datosapp(menu:$menu){
          titulo
          descripcion
          imagen
          tipo
          orden
          usuario
          menu
        }
      }
  """;

const String q2getContactosId = r"""
query getContactosId($idPaciente:String!)
{
  contactosporId(identificacion:$idPaciente)
  {
    identificacion
    tipoIdentificacion
    identificacion
    nombre
    telefono
    celular
    email
    ips
    municipio
    direccion
  }
}

""";

const String q3getContactosIps = """
query contactosIps(\$ips:String!)
{
  contactosIps(ips:\$ips)
  {
    ips
    nombreIps
    tipo
    contacto
    observacion
  }
}
""";

const String q4getPrestadoresPaciente = r"""
query prestadoresPaciente($identificacion:String!)
{
  prestadoresPaciente(identificacion:$identificacion)
  {
    identificacion
    tipoPrestador
    nombrePrestador
  }
}
""";

const String q5getRutasPaciente = r"""
       
      query rutasPaciente($identificacion:String!)
{
  rutasPaciente(identificacion:$identificacion)
  {
    identificacion
    ruta
    procedimiento
    ips
    cumplimiento
    fecha
  }
}
  """;

const String q5getEventosEps = r"""
   query eventosEps($municipio:String!)
{
  eventosEps(municipio:$municipio)
  {
    titulo
    departamento
    municipio
    descripcion
    lugar
    fecha
    horaInicio
    horaFin
    imagen
  }
}
  """;

const String municipios = r"""
query {
  municipios{
    id
    nombre
  }
}
""";

const String estadoAutorizaciones = r"""
query($tipoId:String!, $identificacion:String!){
  autorizaciones(tipoId:$tipoId, identificacion:$identificacion){
    solicitud
    autorizacion
    estado
    fechaAutorizacion
    especialidad
    codServicioEmssanar
    servicio
    codigoUnicoAfiliado
    tipoDocumento
    identificacion
    ipsAutoriza
    codigoHabilitacion
    direccion
    telefono1
    telefono2
    telefonoCitas
  }
}
""";

const String tiposRed =r"""
query{
  tipoRedApp{
    id
    nombreTipoRed
  }
}
""";
