import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class VerPagina extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _VerPagina();
  }

}

class _VerPagina extends State<VerPagina> {
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    final datosappProv = Provider.of<DatosAppProvider>(context);
    return WebviewScaffold(
      url: datosappProv.getDescripcion,
      appBar: AppBar(
        title: Text(datosappProv.getTitulo,
            style: TextStyle(fontSize: 14, fontFamily: 'raleway')),
        backgroundColor: kverde,
      ),
      withZoom: true,
      withLocalStorage: true,
      hidden: true,
      ignoreSSLErrors: true,
      initialChild: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.green[200],
        ),
      ),
    );

    /*Scaffold(
      appBar: AppBar(
        title: Text(datosappProv.getTitulo,
            style: TextStyle(fontSize: 14, fontFamily: 'raleway')),
        backgroundColor: kverde,
      ),
      body: WebView(
        //carga la url que llega por el provider,
        initialUrl: datosappProv.getDescripcion,
        javascriptMode: JavascriptMode.unrestricted,
        onWebResourceError: (error) {
          print("Error en Ver Página" + error.toString());
        },
        onProgress: (value){
          print("VALUE:" + value.toString());
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );*/
  }
}

/* // */
