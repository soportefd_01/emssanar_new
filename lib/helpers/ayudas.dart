import 'package:flutter/material.dart';

//COLORES CORPORATIVOS
const kazulOscuro = Color(0xFF1759a4);
const kazulClaro = Color(0xFF008bd2);
const kgris = Color(0xFFcccccc);
const kverde = Color(0xFFA5CE37);

const defaultPadding = 16.0;

//TITULO AZUL
const ktituloa = TextStyle(
  fontSize: 15,
  fontFamily: 'Raleway',
  color: kazulOscuro,
  fontWeight: FontWeight.w700,
  //fontStyle: FontStyle.italic,
  letterSpacing: 0.5,
);
//TITULO VERDE
const ktitulov = TextStyle(
  fontSize: 15,
  fontFamily: 'Raleway',
  color: kverde,
  fontWeight: FontWeight.bold,
);

//SUBTITULO AZUL
const ksubtituloa = TextStyle(
  fontSize: 10,
  fontWeight: FontWeight.w500,
  fontFamily: 'Raleway',
  color: kazulOscuro,
  //fontWeight: FontWeight.bold,
);
//SUBTITULO VERDE
const ksubtitulov = TextStyle(
  fontSize: 12,
  fontFamily: 'Raleway',
  color: kverde,
  //fontWeight: FontWeight.bold,
);
const kerror = TextStyle(
  fontSize: 9,
  fontFamily: 'Raleway',
  //color: Colors.red,
  //fontWeight: FontWeight.bold,
);

//Estilo titulos del campo
const ktitulocampo =
    TextStyle(color: kazulOscuro, fontFamily: 'Raleway', fontSize: 14);
//Estilo bordes de los campos
const kborderedondeado = OutlineInputBorder(
  borderSide: BorderSide(color: Colors.red, width: 0.5),
  borderRadius: const BorderRadius.all(
    const Radius.circular(12.0),
  ),
);

const kbordenormal = OutlineInputBorder(
  borderSide: BorderSide(color: Color(0xFFA5CE37), width: 0.5),
);
const kbordeseleccionado = OutlineInputBorder(
  borderSide: BorderSide(color: Color(0xFF4CB6F5), width: 0.5),
);

//PADDINGS
Padding pt(double padingarriba) =>
    Padding(padding: EdgeInsets.only(top: padingarriba));

Padding pl(double padingizquierda) =>
    Padding(padding: EdgeInsets.only(left: padingizquierda));

Padding pr(double padingderecha) =>
    Padding(padding: EdgeInsets.only(right: padingderecha));

Padding pb(double padingabajo) =>
    Padding(padding: EdgeInsets.only(bottom: padingabajo));

Padding pvh(double v, double h) =>
    Padding(padding: EdgeInsets.symmetric(vertical: v, horizontal: h));

Padding pltrb(double l, double t, double r, double b) =>
    Padding(padding: EdgeInsets.fromLTRB(l, t, r, b));

Padding pa(double todos) => Padding(padding: EdgeInsets.all(todos));

Icon icono(IconData icono) => Icon(icono, size: 20, color: Colors.red);
Icon iconoVerde(IconData icono) =>
    Icon(icono, size: 20, color: Colors.lightGreen);
Icon iconoExito() => Icon(Icons.check_circle_rounded, color: kverde);
Icon iconoError() => Icon(Icons.error_outlined, color: Colors.red);

SizedBox sbh(double alto) => SizedBox(height: alto);

SizedBox sbw(double ancho) => SizedBox(width: ancho);

SizedBox sbhw(double alto, double ancho) =>
    SizedBox(height: alto, width: ancho);

//linea horizontal o separador
Divider d(double alto) => Divider(
      color: kverde,
      height: alto,
      indent: 10, //donde inicia
      endIndent: 10, //donde termina la linea
      //thickness: 8, //grosor de la liena
    );
