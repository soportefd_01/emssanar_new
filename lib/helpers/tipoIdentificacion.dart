import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

Widget tipoidentificacion(BuildContext context) {
  final loginProv = Provider.of<LoginProvider>(context);
  return Query(
    options: QueryOptions(
        document: gql("query{tipoDocumento{id nombre}}"),
        variables: {},
        fetchPolicy: FetchPolicy.cacheFirst
        /* pollInterval: Duration(seconds: 10), */
        //poll vuelve a realizar la peticion en 10 segundos
        ),
    builder: (result, {refetch, fetchMore}) {
      if (result.hasException) {
        //print(result.exception.toString());
        return Container(
            child: Text('Ocurrio un problema' + result.exception.toString()));
      }
      if (result.isLoading) {
        return Text('Cargando Datos');
      }

      List resultadosConsulta = result.data['tipoDocumento'];
      //print(resultadosConsulta);

      //if (resultadosConsulta == null) {
      if (resultadosConsulta.length == 0) {
        return Text(
          "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
          style: TextStyle(color: Colors.redAccent),
          textAlign: TextAlign.center,
        );
      }
      /* fin validaciones GRAPHQL */

      List<DropdownMenuItem<String>> items = [];
      /* Adiciona una opción al dropdown */
      items.add(DropdownMenuItem(
        child: Padding(
          padding: const EdgeInsets.only(left: 40),
          child: Text(
            "Seleccione:",
            textAlign: TextAlign.center,
            style: ktitulocampo,
          ),
        ),
        value: "",
      ));

      for (int i = 0; i < resultadosConsulta.length; i++) {
        items.add(DropdownMenuItem(
          child: Text(
            resultadosConsulta[i]["nombre"],
            style: ktitulocampo,
          ),
          value: resultadosConsulta[i]["id"],
        ));
      }
      /* return Container(); */
      return Column(
        children: <Widget>[
          DropdownButton<String>(
              value: loginProv.getTipoIdentificacion,
              icon: Icon(Icons.arrow_downward, color: Colors.blue.shade900),
              underline: Container(
                height: 1,
                color: Colors.blue.shade900,
              ),
              /* iconSize: 24,
                  elevation: 16,
                  underline: null, */
              onChanged: (String newValue) {
                loginProv.setTipoIdentificacion = newValue;
              },
              items: items),
        ],
      );

      /* BACKUP ORIGINAL */

      /* return ListView.builder(
          itemCount: resultadosConsulta.length,
          itemBuilder: (context, index) {
            final repository = resultadosConsulta[index];
            return ListTile(
              title: Text(repository['nombre']),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Text("${repository['birthYear']} / ${repository['gender']}"),
                  Text(repository['id'])
                ],
              ),
            );
          }); */
    },
  );
}
