import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Widget toast(BuildContext context, String mensaje, Color colorIcono) {
  /* Toast.show(mensaje, context,
      duration: 5, gravity: Toast.TOP, backgroundColor: Colors.blue.shade900); */
  Fluttertoast.showToast(
      msg: mensaje,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 7,
      backgroundColor: colorIcono,
      textColor: Colors.white
      /* fontSize: 16.0 */
      );
  return Container();
}
