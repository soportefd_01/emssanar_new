import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final _errorLink = ErrorLink(
  onGraphQLError: (request, forward, response) {
    print(response);
    return;
  },
  onException: (request, forward, exception) {
    print(exception);
    return;
  },
);

//'http://190.60.243.113:8081/graphql',

final httpLink = HttpLink(
  'https://crm.emssanar.org.co:8375/graphql',
);
final _httpLink = _errorLink.concat(httpLink);

final Link link = _httpLink;
var client = ValueNotifier(
  GraphQLClient(link: link, cache: GraphQLCache(store: HiveStore())),
);
