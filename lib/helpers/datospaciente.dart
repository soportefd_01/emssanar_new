import 'package:emssanar_new/helpers/ayudas.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/models/datosusuariomodel.dart';

Widget datospaciente(BuildContext context) {
  final loginProv = Provider.of<LoginProvider>(context);
  final contactoProv = Provider.of<ContactosProvider>(context);

  return Query(
    options: QueryOptions(
        document: gql(q2getContactosId),
        variables: {"idPaciente": loginProv.getIdentificacion},
        fetchPolicy: FetchPolicy.cacheFirst),
    builder: (result, {refetch, fetchMore}) {
      if (result.hasException) {
        print(result.exception.toString());
        return Container(child: Text('Ocurrio un problema'));
      }
      if (result.isLoading) {
        return Text('Cargando Datos');
      }

      final user = ContactosporId.fromJson(result.data['contactosporId']);
      //contactoProv.setIps = user.celular.toString();
      contactoProv.setIps = user.ips;
      contactoProv.setMunicipio = user.municipio;
      contactoProv.setIdentificacion = user.identificacion;
      contactoProv.setNombre = user.nombre;
      contactoProv.setCelular = user.celular;
      print("MUNICIPIO: " + user.municipio.toString());
      /* print(" MODELO sin Provider TID: " + user.tipoIdentificacion.toString());
      print(" MODELO sin Provider IDE: " + user.identificacion.toString());
      print(" MODELO sin Provider NOM: " + user.nombre.toString());
      print(" MODELO sin Provider TEL: " + user.telefono.toString());
      print(" MODELO sin Provider CEL: " + user.celular.toString());
      print(" MODELO sin Provider COR: " + user.email.toString());
      print(" MODELO sin Provider DIR: " + user.direccion.toString()); */

      if (result.data['contactosporId'].length == 0) {
        return Text(
          "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
          style: TextStyle(color: Colors.redAccent),
          textAlign: TextAlign.center,
        );
      }
      return Container(
        alignment: Alignment.center,
        child: Column(
            children: [
              Text(
                user.nombre,
                style: ktituloa,
              ),
              pt(10),
              Text(
                user.tipoIdentificacion + " - " + user.identificacion,
                style: ksubtitulov,
              ),
            ],
          ),
      );

      /* fin validaciones GRAPHQL */
    },
  );
}

/* // */
