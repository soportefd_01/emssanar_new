import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/helpers/query.dart';
import 'package:emssanar_new/helpers/ayudas.dart';
//import 'package:emssanar_new/helpers/datospaciente.dart';
import 'package:url_launcher/url_launcher.dart';

Widget datosAppCont(BuildContext context, String menuId) {
  print(menuId);
  return Container(
    height: double.maxFinite,
    //color: Colors.red,
    child: Query(
      options: QueryOptions(
          document: gql(q1getDatosAppAll),
          variables: {"menu": menuId},
          fetchPolicy: FetchPolicy.cacheFirst),
      builder: (QueryResult result,
          {VoidCallback refetch, FetchMore fetchMore}) {

        if (result.hasException) {
          return Text(result.exception.toString());
        }

        if (result.isLoading) {
          return Center(
              child: Text(
            'Cargando Datos',
            style: ktituloa,
          ));
        }

        List datosAppGraphQl = result.data['datosapp'];

        if (datosAppGraphQl.length == 0) {
          return Center(
            child: Text(
              "Sin datos! Reinicie el aplicativo o pongase en contacto con soporte.",
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            ),
          );
        }

        return ListView.builder(
            itemCount: datosAppGraphQl.length,
            itemBuilder: (context, index) {
              final repository = datosAppGraphQl[index];
              return tileMenu(
                  context,
                  repository['titulo'],
                  'subtitulo',
                  repository['imagen'],
                  repository['descripcion'],
                  repository['tipo']);
            });
      },
    ),
  );
}

Widget _tile(String title, String subtitle, String icon, String pagina) => Card(
  elevation: 2,
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  child: ListTile(
    leading: Image.asset(
      "lib/assets/img/icons/" + icon,
      fit: BoxFit.cover,
      alignment: Alignment.center,
    ),
    title: Text(
      title,
      style: ktituloa,
    ),
    trailing: Icon(Icons.arrow_forward),
    onTap: () {
      print('HACER ACCION CON CON PROVIDER');
      QR.to(pagina);
    },
  ),
);

Card tileMenu(BuildContext context, String titulo, String subtitulo,
    String urlimagen, String enlaceweb, String tiporecurso) {
  final datosappProv = Provider.of<DatosAppProvider>(context);
  final loginProv = Provider.of<LoginProvider>(context);
  final contactoProv = Provider.of<ContactosProvider>(context);
  String numero = "";
  String correo = "";
  String whatsapp = "";

  acciones(String cadena) async {
    if (await canLaunch(cadena)) {
      await launch(cadena);
    } else {
      throw 'No se completar el proceso ' + cadena;
    }
  }

  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ListTile(
      leading: Image.network(
        urlimagen,
        //fit: BoxFit.cover,
        height: 30,
        width: 30,
      ),
      //leading: FlutterLogo(),
      trailing: Icon(Icons.arrow_forward),
      title: Text(
        titulo,
        //textAlign: TextAlign.center,
        style: ktituloa,
      ),
      onTap: () async => {
        datosappProv.setDescripcion = enlaceweb,
        datosappProv.setTitulo = titulo,
        datosappProv.setTipo = tiporecurso,
        if (datosappProv.getTipo == 'L')
          {
            QR.to('/verpaginaRuta'),
          }
        else if (datosappProv.getTipo == 'I')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'V')
          {
            print("Pendiente por CREAR RUTA Y WIDGET"),
          }
        else if (datosappProv.getTipo == 'N')
          {
            numero = "tel:" + datosappProv.getDescripcion.toString(),
            acciones(numero),
          }
        else if (datosappProv.getTipo == 'R')
          {
            QR.to('/' + datosappProv.getDescripcion.toString()),
          }
        else if (datosappProv.getTipo == 'S')
          {
            whatsapp = "https://wa.me/+57" +
                datosappProv.getDescripcion.toString() +
                "?text=\n¡Hola! \n\n" +
                contactoProv.getNombre.toString() +
                "\n" +
                loginProv.getTipoIdentificacion.toString() +
                " - " +
                loginProv.getIdentificacion.toString() +
                "\nCelular: " +
                datosappProv.getDescripcion.toString() +
                "\n\nPor favor interactua con Clara Bot.",
            acciones(whatsapp),
          }
        else if (datosappProv.getTipo == 'C')
          {
            correo = "mailto:" +
                datosappProv.getDescripcion +
                "?subject=Solicitud desde APP&body=\nSeñores: \n\nEMSSANAR EPS. " +
                "\n\nPor favor  \n.\n.\n.\n. \n\nAtentamente,\n\n" +
                contactoProv.getNombre +
                "\n" +
                loginProv.getTipoIdentificacion +
                " : " +
                loginProv.getIdentificacion +
                "\nCelular: " +
                contactoProv.getCelular +
                "\n\nMuchas gracias.",
            acciones(correo),
          }
        else
          {
            print("OPCION NO VALIDA"),
          }
      },
    ),
  );
}
