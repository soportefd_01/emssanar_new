class ValidacionCampo {
  final String valor;
  final String error;
  final bool esCorrecto;
  ValidacionCampo(this.valor, this.error, this.esCorrecto);
}

/* class ValidacionCampoBool {
  bool valorCheck = false;
  final String error2;
  ValidacionCampoBool(this.valorCheck, this.error2);
} */

/* class ValidationItem {
  final String value;
  final String error;

  ValidationItem(this.value, this.error);
}
 */

/* // */
