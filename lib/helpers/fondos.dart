import 'package:flutter/material.dart';
import 'package:emssanar_new/helpers/ayudas.dart';

class Figuras extends StatelessWidget {
  const Figuras({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //body: HeaderCuadrado(),
      //body: HeaderBordesRedondeados(),
      //body: HeaderDiagonal(),
      //body: HeaderTriangular(),
      //body: HeaderPico(),
      //body: HeaderCurvo(),
      //body: HeaderWaves(),
      appBar: AppBar(
        title: Text('Figuras'),
        backgroundColor: Color.fromARGB(145, 20, 120, 2),
      ),
      body: FondoEmssanar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/inicioRuta');
        },
        child: Icon(Icons.home),
        backgroundColor: Colors.indigo,
      ),
    );
  }
}

//Fondo emssanar
class FondoEmssanar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        //EmssanarWaves(),
        //ImagenEmssanar(),
        //FlechaAbajo(),
        HeaderWaves(),
      ],
    );
  }
}

//container inicio emssanar
class EmssanarWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: CustomPaint(
        painter: _EmssanarWavesPainter(),
      ),
    );
  }
}

class _EmssanarWavesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapizPintar = Paint();
    final coordenadas = Path();

    //Propiedade
    lapizPintar.color = kverde; //color
    //lapizPintar.color = Color(0Xff615AAB); //color
    //lapizPintar.color = Color.fromRGBO(165, 206, 55, 1); //color
    //lapizPintar.style = PaintingStyle.stroke; //stroke dibujar lineas
    lapizPintar.style = PaintingStyle.fill; //Dibujar o Rellenar
    lapizPintar.strokeWidth = 10; //ancho del lapiz

    //PRIMERA FORMA
    //Diubjamos con el coordenadas y el lapiz
    coordenadas.lineTo(0, size.height * 0.55);

    coordenadas.quadraticBezierTo(size.width * 0.30, size.height * 0.5,
        size.width * 0.30, size.height * 0.30);

    coordenadas.quadraticBezierTo(
        size.width * 0.30, size.height * 0.10, size.width * 0.9, 0);

    coordenadas.lineTo(0, 0);

    /* figura inferior */
    coordenadas.moveTo(0, size.height);
    coordenadas.lineTo(0, size.height);
    coordenadas.lineTo(size.width, size.height);
    coordenadas.lineTo(size.width, size.height * 0.95);
    /* coordenadas.quadraticBezierTo(size.width * 0.65, size.height * 0.4,
        size.width * 0.65, size.height * 0.6); */

    coordenadas.quadraticBezierTo(
      size.width * 0.55,
      size.height * 0.9,
      0,
      size.height,
    );

    coordenadas.lineTo(0, size.height);

    canvas.drawPath(coordenadas, lapizPintar);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    throw false;
  }
}

//container CURVO INVERTIDO DOS PENDIENTES SENO COCENO
class HeaderWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      //color: Colors.red,
      child: CustomPaint(
        painter: _HeaderWavesPainter(),
      ),
    );
  }
}

class _HeaderWavesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapizPintar = Paint();
    final coordenadas = Path();

    //Propiedade
    lapizPintar.color = Color(0xFFa5ce37); //color
    //lapizPintar.style = PaintingStyle.stroke; //stroke dibujar lineas
    lapizPintar.style = PaintingStyle.fill; //Dibujar o Rellenar
    lapizPintar.strokeWidth = 20; //ancho del lapiz

    //PRIMERA FORMA
    //Diubjamos con el coordenadas y el lapiz
    coordenadas.lineTo(0, size.height * 0.25); //liena 1

    //PENDIENTE HACIA ABAJO
    coordenadas.quadraticBezierTo(size.width * 0.25, size.height * 0.30,
        size.width * 0.5, size.height * 0.25);

    //PENDIENTE HACIA ARRIBA
    coordenadas.quadraticBezierTo(
        size.width * 0.75, size.height * 0.20, size.width, size.height * 0.25);

    //coordenadas.lineTo(size.width, size.height * 0.5); //liena 1
    coordenadas.lineTo(size.width, 0); //liena 1

    canvas.drawPath(coordenadas, lapizPintar);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    throw false;
  }
}
