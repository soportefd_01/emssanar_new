//import 'package:emssanar_new/validations/validationCampo.dart';
import 'dart:io';
import 'package:emssanar_new/providers/interaccionappprovider.dart';
import 'package:flutter/material.dart';
import 'package:qlevar_router/qlevar_router.dart';
import 'rutas/mis_rutas.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:emssanar_new/providers/loginprovider.dart';
import 'package:emssanar_new/providers/datosAppProvider.dart';
import 'package:emssanar_new/providers/datospacienteprovider.dart';
import 'package:emssanar_new/providers/interaccioncovidprovider.dart';
import 'package:emssanar_new/providers/contactosRedProvider.dart';
import 'package:emssanar_new/helpers/cliente.dart';

//para confuguración certificado.
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
//fin confuguración certificado.

void main() async {
  QR.settings.enableDebugLog = true; //para manejo de rutas
  await initHiveForFlutter(); //Para graphql
  HttpOverrides.global =
      new MyHttpOverrides(); // Para permitir certificado ssl mal configurado
  runApp(MyApp(client: client));
}

class MyApp extends StatelessWidget {
  final ValueNotifier<GraphQLClient> client;

  const MyApp({Key key, this.client}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => LoginProvider()),
          ChangeNotifierProvider(create: (_) => DatosAppProvider()),
          ChangeNotifierProvider(create: (_) => ContactosProvider()),
          ChangeNotifierProvider(create: (_) => InteraccionCovidProvider()),
          ChangeNotifierProvider(create: (_) => InteraccionAppProvider()),
          ChangeNotifierProvider(create: (_) => RedProvider()),
        ],
        child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            brightness: Brightness.light,
            primaryColor: Colors.blue.shade900,
          ),
          routeInformationParser: QRouteInformationParser(),
          routerDelegate: QRouterDelegate(
            AppRoutes().routes(),
          ),
        ),
      ),
    );
  }
}
